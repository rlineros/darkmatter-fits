"""
Prints all function for direct detection contraints
"""

if __name__=="__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.pylab as pylab
    import dmfits as dmf

    print("\n*) Functions for Spin-Independent Cross Section:\n")
    for ff in dir(dmf):
        if "si" in ff:
            print("dmf."+ff)
            print(getattr(dmf, ff).__doc__)
            print()

    print("\n*) Functions for Spin-Dependent Cross Section intro protons:\n")
    for ff in dir(dmf):
        if "sdp" in ff:
            print("dmf."+ff)
            print(getattr(dmf, ff).__doc__)
            print()

    print("\n*) Functions for Spin-Dependent Cross Section intro neutrons:\n")
    for ff in dir(dmf):
        if "sdn" in ff:
            print("dmf."+ff)
            print(getattr(dmf, ff).__doc__)
            print()

