# Dark Matter Fits

Fitting functions for Dark Matter direct and indirect detection results.

The code is written in python 3.

*  Direct Detection fits
*  Indirect Detection fits

Author: Roberto A. Lineros gitlab: @rlineros

This project just started and new fitting function will be added soon! Comments are welcome!


### Basic example

    import dmfits as dmf

    mdm = [1,30,45,90,1e3] # array with values of DM mass to be evaluates
    dmf.id_ww_cta1709(mdm) # array with the value of the exclusion curve

The function **dmf.id_ww_cat1709** returns an array with the values of the curve reported in the paper.


### Fits for Indirect Detection

* **dmf.id_bb_DLTorre_2401**  Annihilation cross section into b-quarks [cm^3 s^-1] ref: 2401.10329 
* **dmf.id_bb_cta_1508**  Annihilation cross section into bottom quarks [cm^3 s^-1] ref: 1508.06128 
* **dmf.id_bb_cta_1709**  Annihilation cross section into b-quarks [cm^3 s^-1] ref: 1709.07997 
* **dmf.id_bb_cui_1803**  Annihilation cross section into b-quarks [cm^3 s^-1] ref: 1803.02163 
* **dmf.id_bb_hess_2207**  Annihilation cross section into tau [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_bb_km3netantares_icrc2021**  Annihilation cross section into b quarks  [cm^3 s^-1] ref: icrc2021.537 
* **dmf.id_bb_magicfermi_1601**  Annihilation cross section into b-quark [cm^3 s^-1] ref: 1601.06590 
* **dmf.id_bb_swgo_icrc2021**  Annihilation cross section into b-quarks [cm^3 s^-1] ref: ICRC2021_555 
* **dmf.id_ee_hess_2207**  Annihilation cross section into e+ e- [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_hh_hess_2207**  Annihilation cross section into hh [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_mu_hess_2207**  Annihilation cross section into muons [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_mu_km3netantares_icrc2021**  Annihilation cross section into muons  [cm^3 s^-1] ref: icrc2021.537 
* **dmf.id_mu_magicfermi_1601**  Annihilation cross section into muons [cm^3 s^-1] ref: 1601.06590 
* **dmf.id_nu_km3netantares_icrc2021**  Annihilation cross section into tau  [cm^3 s^-1] ref: icrc2021.537 
* **dmf.id_tau_cta_1508**  Annihilation cross section into taus [cm^3 s^-1] ref: 1508.06128 
* **dmf.id_tau_cta_1709**  Annihilation cross section into tau pairs [cm^3 s^-1] ref: 1709.07997 
* **dmf.id_tau_hess_1711**  Annihilation cross section into tau [cm^3 s^-1] ref: 1711.08634 
* **dmf.id_tau_hess_2207**  Annihilation cross section into tau [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_tau_km3netantares_icrc2021**  Annihilation cross section into tau  [cm^3 s^-1] ref: icrc2021.537 
* **dmf.id_tau_magicfermi_1601**  Annihilation cross section into tau-lepton [cm^3 s^-1] ref: 1601.06590 
* **dmf.id_tau_swgo_icrc2021**  Annihilation cross section into tau [cm^3 s^-1] ref: ICRC2021_555 
* **dmf.id_tt_cta_1508**  Annihilation cross section into top quarks [cm^3 s^-1] ref: 1508.06128 
* **dmf.id_tt_cta_1709**  Annihilation cross section into top quarks [cm^3 s^-1] ref: 1709.07997 
* **dmf.id_tt_hess_2207**  Annihilation cross section into t-quark [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_ww_cta_1508**  Annihilation cross section into W-boson [cm^3 s^-1] ref: 1508.06128 
* **dmf.id_ww_cta_1709**  Annihilation cross section into W-boson [cm^3 s^-1] ref: 1709.07997 
* **dmf.id_ww_hess_1711**  Annihilation cross section into W-boson [cm^3 s^-1] ref: 1711.08634 
* **dmf.id_ww_hess_2207**  Annihilation cross section into tau [cm^3 s^-1] ref: 2207.10471 
* **dmf.id_ww_km3netantares_icrc2021**  Annihilation cross section into W-boson [cm^3 s^-1] ref: icrc2021.537 
* **dmf.id_ww_magicfermi_1601**  Annihilation cross section into W-boson [cm^3 s^-1] ref: 1601.06590 
* **dmf.id_ww_swgo_icrc2021**  Annihilation cross section into W-boson [cm^3 s^-1] ref: ICRC2021_555

### Fits for Direct Detection
#### Functions for Spin-Independent Cross Section:

* **dmf.si_cdmslite_1509** Spin independent DM-Nucleon [cm^2] ref: 1509.02448
* **dmf.si_darwin_1606** Spin independent DM-Nuclean [cm^2] ref: 1606.07001
* **dmf.si_edelweiss_2211** Spin independent DM-Nucleon [cm^2] ref: 2211.04176
* **dmf.si_lux_1608** Spin independent DM-Nuclean [cm^2] ref: 1608.07648
* **dmf.si_lz_2207** Spin independent DM-Nucleon [cm^2] ref: 2207.03764
* **dmf.si_neutrinofloor_1307** Neutrino Floor Spin independent DM-Nucleon [cm^2] ref: 1307.5458
* **dmf.si_pandax4t_2107** Spin independent DM-Nucleon [cm^2] ref: 2107.13438
* **dmf.si_picassofinal_1611** Spin independent DM-Nucleon [cm^2] ref: 1611.01499
* **dmf.si_pico60_1702** Spin independent DM-nucleon [cm^2] ref: 1702.07666
* **dmf.si_xenon100_1609** Spin independent DM-Nuclean [cm^2] ref: 1609.06154
* **dmf.si_xenon1t_1705** Spin independent DM-Nuclean [cm^2] ref: 1705.06655
* **dmf.si_xenon1t_1805** Spin independent DM-Nuclean [cm^2] ref: 1805.12562
* **dmf.si_xenonnT_2303** Spin independent DM-Nucleon [cm^2] ref: 2303.14729
* **dmf.si_xlzd_18patras** Spin independent DM-Nucleon [cm^2] ref: 18th Patras Workshop


#### Functions for Spin-Dependent Cross Section intro protons:

* **dmf.sdp_lux_1602** Spin dependent DM-proton [cm^2] ref: 1602.03489
* **dmf.sdp_lz_2207** Spin dependent DM-proton cross-section [cm^2] ref: 2207.03764
* **dmf.sdp_picassofinal_1611** Spin dependent DM-proton [cm^2] ref: 1611.01499
* **dmf.sdp_pico60_1702** Spin dependent DM-proton [cm^2] ref: 1702.07666
* **dmf.sdp_xenon100_1609** Spin dependent DM-proton [cm^2] ref: 1609.06154
* **dmf.sdp_xenonnT_2303** Spin dependent DM-proton cross-section [cm^2] ref: 2303.14729

#### Functions for Spin-Dependent Cross Section intro neutrons:

* **dmf.sdn_darwin_1606** Spin dependent DM-Neutron [cm^2] ref: 1606.07001
* **dmf.sdn_lux_1602** Spin dependent DM-neutron [cm^2] ref: 1602.03489
* **dmf.sdn_lz_2207** Spin dependent DM-neutron cross-section [cm^2] ref: 2207.03764
* **dmf.sdn_xenon100_1609** Spin dependent DM-neutron [cm^2] ref: 1609.06154
* **dmf.sdn_xenonnt_2303** Spin dependent DM-neutron cross-section [cm^2] ref: 2303.14729