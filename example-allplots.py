#!/usr/bin/env python
"""
dmfits.py

author: Roberto A. Lineros

This file contains examples on how to use the fitting functions of DM detection for plots SIGMA vs MDM
"""

if __name__=="__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.pylab as pylab

    pylab.rcParams['figure.figsize'] = 8, 5  # that's default image size for this interactive session
    pylab.rcParams['text.usetex'] = True
    pylab.rcParams['lines.antialiased'] = True
    pylab.rcParams['font.family'] = 'serif'

    ###############################################
    import dmfits as dmf

    print("*********************************")
    print("dmf: Dark Matter Fits module")
    print("Generating direct detection plots \n using fitting functions as an \n example of how to use the module")
    print("*********************************")


    dd_sdp = []
    for ff in dir(dmf):
        if "sdp" in ff:
            dd_sdp.append(ff)

    dd_sdn = []
    for ff in dir(dmf):
        if "sdn" in ff:
            dd_sdn.append(ff)

    dd_si = []
    for ff in dir(dmf):
        if "si" in ff:
            dd_si.append(ff)

    # PLOTS DIRECT DETECTION SPIN DEPENDENT: PROTON
    mdmmin = 1e-1 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\sigma_{SD, p} [{\rm cm}^2]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title(r'DM Direct Detection: Spin-dependent proton')
    plt.xscale('log')
    plt.yscale('log')

    for ii in dd_sdp:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    # PLOT DIRECT DETECTION SPIN DEPENDENT: NEUTRON
    mdmmin = 1e-1 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)


    plt.ylabel(r'$\sigma_{SD,n} [{\rm cm}^2]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title(r'DM Direct Detection: Neutron Spin-dependent cross-section')
    plt.xscale('log')
    plt.yscale('log')

    for ii in dd_sdn:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    # PLOT DIRECT DETECTION SPIN INDEPENDENT
    mdmmin = 1e-1 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\sigma_{\rm SI}  [{\rm cm}^2]$', fontsize=14)
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$', fontsize=14)
    plt.title('Spin Independent Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    for ii in dd_si:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    # ANNIHILATION CROSS SECTION
    id_bb = []
    for ff in dir(dmf):
        if "bb" in ff:
            id_bb.append(ff)

    id_mu = []
    for ff in dir(dmf):
        if "mu" in ff:
            id_mu.append(ff)

    id_ww = []
    for ff in dir(dmf):
        if "ww" in ff:
            id_ww.append(ff)

    id_tau = []
    for ff in dir(dmf):
        if "tau" in ff:
            id_tau.append(ff)

    id_nu = []
    for ff in dir(dmf):
        if "nu" in ff:
            id_nu.append(ff)

    id_tt = []
    for ff in dir(dmf):
        if "tt" in ff:
            id_tt.append(ff)

    id_ee = []
    for ff in dir(dmf):
        if "ee" in ff:
            id_ee.append(ff)

    id_2phot = []
    for ff in dir(dmf):
        if "2phot" in ff:
            id_2phot.append(ff)

    id_hh = []
    for ff in dir(dmf):
        if "hh" in ff:
            id_hh.append(ff)


    ### W+W-

    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{W^+W^-} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_ww:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### TAU
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{\tau^+\tau^-} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_tau:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### B-QUARKS
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{b\overline{b}} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_bb:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### MUONS
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{\mu^+\mu^-} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_mu:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()


    #### NEUTRINOS
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{\nu\nu} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_nu:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### TOP QUARK
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\sigma_{t\overline{t}} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_tt:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### ELECTRONS
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{e^+e^-} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_ee:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### 2 PHOTONS
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{\gamma \gamma} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_2phot:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    #### HIGGS BOSON
    mdmmin = 1e0 # GeV
    mdmmax = 1e6 # GeV

    mdm = np.logspace(np.log10(mdmmin),np.log10(mdmmax),300)

    plt.ylabel(r'$\langle \sigma v \rangle_{hh} [{\rm cm}^3 {\rm s}^{-1}]$')
    plt.xlabel(r'${\rm DM \ mass \ [GeV]}$')
    plt.title('DM Annihilation Cross Section')
    plt.xscale('log')
    plt.yscale('log')

    plt.plot([mdmmin, mdmmax],[3e-26, 3e-26], linestyle="dashed", color="black", label="Freeze-out")

    for ii in id_hh:
        hh = getattr(dmf, ii)
        plt.plot(mdm, hh(mdm,extrapol=False)[0], label=hh(100)[3])

    plt.legend()
    plt.show()
    plt.close()

    plt.show()
