"""
Prints all function for direct detection contraints
"""

if __name__=="__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.pylab as pylab
    import dmfits as dmf

    print("\n*) Functions for Indirect Detection\n")

    for ff in dir(dmf):
        if "id" in ff:
            print("dmf."+ff, getattr(dmf, ff).__doc__)

    print("\n")
    print("Example: import dmfits as dmf\n")
    print("\timport dmfits as dmf\n")
    print("\tmdm = [1,30,45,90,1e3]")
    print("\tdmf.id_ww_cta1709(mdm, Extrapol=False)\n")
    print("Return an array with the values of the curve reported in the paper")


    #print(dmf.id_bb_cui_1803.label)
