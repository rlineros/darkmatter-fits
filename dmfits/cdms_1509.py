""" cdms.py constains fit for results of CDMSlite """

### last modification: 26/feb/2023

import numpy as np

# Data taken from 1509.02448
@np.vectorize
def si_cdmslite_1509(mdm : float = 100, extrapol : bool = False) -> float:
    """ Spin independent DM-Nucleon [cm^2] ref: 1509.02448 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"CDMS-Lite 1509.02448"
    mass_min = 1.45744E+00
    mass_max = 1.50081E+01
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -9.51568E+01
    fmax = -8.51892E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [-5.92787E-02, 5.84872E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 6.61356E-03, 1.26855E-02, 1.91308E-02, 2.65201E-02, 3.27234E-02, 4.01480E-02, 4.66253E-02, 5.42950E-02, 6.23093E-02, 7.12161E-02, 8.04291E-02, 9.02734E-02, 9.79944E-02, 1.09053E-01, 1.20403E-01, 1.32960E-01, 1.45872E-01, 1.59418E-01, 1.75716E-01, 1.90828E-01, 2.05940E-01, 2.22907E-01, 2.37768E-01, 2.53852E-01, 2.72653E-01, 2.88754E-01, 3.06691E-01, 3.25250E-01, 3.41696E-01, 3.62104E-01, 3.86501E-01, 4.15183E-01, 4.44204E-01, 4.71098E-01, 4.92808E-01, 5.15126E-01, 5.37461E-01, 5.63143E-01, 5.89112E-01, 6.13535E-01, 6.38219E-01, 6.65365E-01, 6.93754E-01, 7.10540E-01, 7.31912E-01, 7.53287E-01, 7.78934E-01, 8.02767E-01, 8.28733E-01, 8.48606E-01, 8.73973E-01, 9.08831E-01, 9.36962E-01, 9.57763E-01, 9.76132E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.46355E-01, 9.01583E-01, 8.65280E-01, 8.32202E-01, 8.03965E-01, 7.75324E-01, 7.43053E-01, 7.06749E-01, 6.75284E-01, 6.40591E-01, 6.05898E-01, 5.73625E-01, 5.43773E-01, 5.10288E-01, 4.74786E-01, 4.37267E-01, 4.05797E-01, 3.77149E-01, 3.48095E-01, 3.23881E-01, 2.99668E-01, 2.77872E-01, 2.60515E-01, 2.43157E-01, 2.20956E-01, 2.05614E-01, 1.90270E-01, 1.76136E-01, 1.65633E-01, 1.53110E-01, 1.41792E-01, 1.30470E-01, 1.23180E-01, 1.17909E-01, 1.15064E-01, 1.11814E-01, 1.10581E-01, 1.06925E-01, 1.00848E-01, 9.27569E-02, 7.90192E-02, 6.68921E-02, 5.71834E-02, 5.11166E-02, 4.42383E-02, 3.77633E-02, 2.96707E-02, 2.44031E-02, 1.79233E-02, 1.50796E-02, 1.02136E-02, 5.74109E-03, 2.08214E-03, 4.47296E-04, 8.31454E-04, 0.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(si_cdmslite_1509,"label", r"CDMS-Lite 1509.02448")
setattr(si_cdmslite_1509,"url", "https://inspirehep.net/literature/1392432")
setattr(si_cdmslite_1509,"mass_min", 1.45744E+00)
setattr(si_cdmslite_1509,"mass_max", 1.50081E+01)