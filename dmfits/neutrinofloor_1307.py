""" FITTING FUNCTIONS FOR NEUTRINO FLOOR """

import numpy as np

# Data taken from 1307.5458
@np.vectorize
def si_neutrinofloor_1307(mdm = 100, extrapol=False):
    """ Neutrino Floor Spin independent DM-Nucleon [cm^2] ref: 1307.5458 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"Neutrino floor 1307.5458"
    mass_min = 3.01602E-01
    mass_max = 9.99635E+03
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.13061E+02
    fmax = -9.94819E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.81097E-01, -3.79572E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.39314E-02, 4.12706E-02, 6.84498E-02, 9.59107E-02, 1.23395E-01, 1.52037E-01, 1.77866E-01, 2.03084E-01, 2.33813E-01, 2.60778E-01, 2.87721E-01, 3.13662E-01, 3.23308E-01, 3.31062E-01, 3.36590E-01, 3.39252E-01, 3.44464E-01, 3.49449E-01, 3.66804E-01, 3.90905E-01, 4.15362E-01, 4.47341E-01, 4.74687E-01, 5.01659E-01, 5.29390E-01, 5.56739E-01, 5.84455E-01, 6.11441E-01, 6.38788E-01, 6.67684E-01, 6.93491E-01, 7.20842E-01, 7.52159E-01, 7.76417E-01, 8.02887E-01, 8.32359E-01, 8.59356E-01, 8.85665E-01, 9.14059E-01, 9.41409E-01, 9.64937E-01, 9.91258E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.72537E-01, 9.70888E-01, 9.44166E-01, 8.93590E-01, 8.32063E-01, 7.83900E-01, 7.68096E-01, 7.57091E-01, 7.73296E-01, 7.88266E-01, 7.84851E-01, 7.24831E-01, 6.18821E-01, 5.07496E-01, 4.11162E-01, 3.26655E-01, 2.38832E-01, 1.48244E-01, 5.07111E-02, 1.47988E-02, 1.84840E-03, 0.00000E+00, 1.13299E-02, 3.12877E-02, 5.56907E-02, 7.35982E-02, 9.83497E-02, 1.14927E-01, 1.29569E-01, 1.53314E-01, 1.73026E-01, 1.95112E-01, 2.10324E-01, 2.20090E-01, 2.43283E-01, 2.71658E-01, 2.90394E-01, 3.07927E-01, 3.35144E-01, 3.55338E-01, 3.73265E-01, 3.93668E-01, 4.03099E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label