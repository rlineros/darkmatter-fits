"""constains fit for results of AMS02 antiproton bounds """

### last modification: 13/jun/2023

import numpy as np

# Data taken from 1803.02163
@np.vectorize
def id_bb_cui_1803(mdm : float = 200, extrapol : bool = False) -> float:
    """ Annihilation cross section into b-quarks [cm^3 s^-1] ref: 1803.02163 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 1.00000E+01
    mass_max = 9.96507E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.32706E+01
    fmax = -5.50840E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.32115E+00, -3.20937E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.67258E-02, 3.34516E-02, 5.47390E-02, 7.60264E-02, 9.57932E-02, 1.13026E-01, 1.28738E-01, 1.43436E-01, 1.57121E-01, 1.69792E-01, 1.81956E-01, 1.94121E-01, 2.06792E-01, 2.19970E-01, 2.34668E-01, 2.51394E-01, 2.70654E-01, 2.91941E-01, 3.13229E-01, 3.34516E-01, 3.55803E-01, 3.76077E-01, 3.93817E-01, 4.10035E-01, 4.28789E-01, 4.50076E-01, 4.71363E-01, 4.92651E-01, 5.13938E-01, 5.35226E-01, 5.56513E-01, 5.77800E-01, 5.99088E-01, 6.20375E-01, 6.41662E-01, 6.62950E-01, 6.84237E-01, 7.05525E-01, 7.26812E-01, 7.48099E-01, 7.69387E-01, 7.90674E-01, 8.11961E-01, 8.33249E-01, 8.54536E-01, 8.75824E-01, 8.97111E-01, 9.18398E-01, 9.39686E-01, 9.60973E-01, 9.82261E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 1.55784E-02, 3.29871E-02, 6.06538E-02, 9.51732E-02, 1.32722E-01, 1.69879E-01, 2.06563E-01, 2.44147E-01, 2.81876E-01, 3.18592E-01, 3.55538E-01, 3.93452E-01, 4.31079E-01, 4.68620E-01, 5.05741E-01, 5.43631E-01, 5.81042E-01, 6.14412E-01, 6.38589E-01, 6.50847E-01, 6.45824E-01, 6.19536E-01, 5.79204E-01, 5.42053E-01, 5.04810E-01, 4.77909E-01, 4.65395E-01, 4.63608E-01, 4.70290E-01, 4.82464E-01, 4.98255E-01, 5.16600E-01, 5.35626E-01, 5.55631E-01, 5.76190E-01, 5.97344E-01, 6.19052E-01, 6.41270E-01, 6.63829E-01, 6.86729E-01, 7.10011E-01, 7.33975E-01, 7.58662E-01, 7.84328E-01, 8.10633E-01, 8.37533E-01, 8.64817E-01, 8.92483E-01, 9.20363E-01, 9.48583E-01, 9.77157E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_bb_cui_1803,"label", r"Cui et al. $b \bar{b}$ 1803.02163")
setattr(id_bb_cui_1803,"url", "https://inspirehep.net/literature/1658810")
setattr(id_bb_cui_1803,"mass_min", 1.00000E+01)
setattr(id_bb_cui_1803,"mass_max", 9.96507E+03)
