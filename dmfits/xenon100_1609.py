""" FITTING FUNCTIONS FOR XENON100"""

import numpy as np

# Data taken from 1609.06154

@np.vectorize
def sdn_xenon100_1609(mdm = 100, extrapol=False):
    """ Spin dependent DM-neutron [cm^2] ref: 1609.06154 """
    """ extrapol : if True the fitting function return values beyond max and min """
    label = "XENON100 SDn 1609.06154"
    mass_min = 8.21185E+00
    mass_max = 1.01319E+03
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.13822E+01
    fmax = -8.50862E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.56624E-01, -3.52791E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 8.52267E-03, 1.95200E-02, 3.05396E-02, 4.24248E-02, 5.50979E-02, 7.19936E-02, 9.26569E-02, 1.13376E-01, 1.26182E-01, 1.52466E-01, 1.84543E-01, 2.22275E-01, 2.70482E-01, 3.26458E-01, 3.75742E-01, 4.39547E-01, 4.96156E-01, 5.23611E-01, 5.91689E-01, 6.51754E-01, 7.16919E-01, 7.88831E-01, 8.67118E-01, 9.25103E-01, 9.86039E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.27888E-01, 8.43772E-01, 7.64471E-01, 6.89998E-01, 6.03497E-01, 5.18259E-01, 4.27053E-01, 3.47887E-01, 2.90280E-01, 2.29249E-01, 1.44221E-01, 9.65896E-02, 3.46576E-02, 1.13560E-02, 0.00000E+00, 2.85717E-02, 5.10245E-02, 6.10354E-02, 1.01705E-01, 1.42263E-01, 1.91319E-01, 2.40468E-01, 3.00540E-01, 3.47088E-01, 3.93678E-01, 4.03502E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def sdp_xenon100_1609(mdm = 100, extrapol=False):
    """ Spin dependent DM-proton [cm^2] ref: 1609.06154 """
    """ extrapol : if True the fitting function return values beyond max and min """
    label = "XENON100 SDp 1609.06154"
    mass_min = 8.10277E+00
    mass_max = 9.99853E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -8.81008E+01
    fmax = -8.15911E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.11805E-01, -3.62034E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.62946E-02, 2.87640E-02, 4.31655E-02, 5.70875E-02, 7.24763E-02, 9.41287E-02, 1.13880E-01, 1.38466E-01, 1.64512E-01, 2.09426E-01, 2.50994E-01, 2.98397E-01, 3.52122E-01, 4.08768E-01, 4.61566E-01, 5.29393E-01, 6.19526E-01, 6.87864E-01, 7.52321E-01, 8.09516E-01, 8.70104E-01, 9.22933E-01, 9.61225E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 8.76571E-01, 7.88571E-01, 6.97143E-01, 6.09143E-01, 5.31429E-01, 4.30857E-01, 3.56571E-01, 2.76571E-01, 2.02286E-01, 1.15429E-01, 6.05714E-02, 2.28571E-02, 5.71428E-03, 0.00000E+00, 1.25714E-02, 3.88571E-02, 8.80000E-02, 1.33714E-01, 1.73714E-01, 2.14857E-01, 2.59429E-01, 2.94857E-01, 3.22286E-01, 3.49714E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def si_xenon100_1609(mdm = 100, extrapol=False):
    """ Spin independent DM-Nuclean [cm^2] ref: 1609.06154 """
    """ extrapol : if True the fitting function return values beyond max and min """
    label="XENON100 SI 1609.06154"
    mass_min = 5.46549E+00
    mass_max = 9.61055E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.03454E+02
    fmax = -9.18256E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [4.54308E-01, -2.39372E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 6.62595E-03, 1.32140E-02, 1.98273E-02, 2.55155E-02, 3.44479E-02, 4.89182E-02, 6.18934E-02, 7.37287E-02, 8.55514E-02, 1.00986E-01, 1.16357E-01, 1.36228E-01, 1.57772E-01, 1.79797E-01, 2.03965E-01, 2.23887E-01, 2.50564E-01, 2.88243E-01, 3.27658E-01, 3.61891E-01, 4.10356E-01, 4.53346E-01, 5.01990E-01, 5.40431E-01, 5.86387E-01, 6.30494E-01, 6.69746E-01, 7.15640E-01, 7.56210E-01, 7.96375E-01, 8.39556E-01, 8.79658E-01, 9.12725E-01, 9.42699E-01, 9.67833E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.39482E-01, 8.81636E-01, 8.22009E-01, 7.65047E-01, 6.98311E-01, 6.16472E-01, 5.46202E-01, 4.93736E-01, 4.42161E-01, 3.86158E-01, 3.34608E-01, 2.78636E-01, 2.29801E-01, 1.78298E-01, 1.32153E-01, 1.03792E-01, 6.83524E-02, 3.92246E-02, 1.27810E-02, 7.67666E-03, 0.00000E+00, 2.97283E-03, 1.40011E-02, 2.49579E-02, 3.77486E-02, 5.58703E-02, 7.21767E-02, 8.94203E-02, 1.06627E-01, 1.21158E-01, 1.41945E-01, 1.60930E-01, 1.75411E-01, 1.88981E-01, 1.99845E-01, 2.15211E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

