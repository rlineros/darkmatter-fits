""" FITTING FUNCTIONS FOR XENON1T """

import numpy as np

# Data taken from 1705.06655

@np.vectorize
def si_xenon1t_1705(mdm = 100, extrapol=False):
    """ Spin independent DM-Nuclean [cm^2] ref: 1705.06655 """
    """ extrapol : if True the fitting function return values beyond max and min """
    label = "XENON1T SI 1705.06655"
    mass_min = 6.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.06177E+02
    fmax = -9.85813E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [9.76643E-01, -3.01827E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.07790E-02, 3.87786E-02, 5.46553E-02, 6.88576E-02, 9.34339E-02, 1.23513E-01, 1.62292E-01, 2.16947E-01, 2.55725E-01, 2.85804E-01, 3.31160E-01, 3.79238E-01, 4.03815E-01, 4.33894E-01, 4.72672E-01, 5.66106E-01, 6.89619E-01, 7.83053E-01, 8.37708E-01, 9.06566E-01, 9.51921E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 7.93908E-01, 6.40353E-01, 5.21915E-01, 4.28670E-01, 2.93618E-01, 1.69270E-01, 6.56747E-02, 3.45517E-03, 0.00000E+00, 1.05651E-02, 3.63869E-02, 7.27998E-02, 9.42340E-02, 1.22445E-01, 1.60014E-01, 2.51057E-01, 3.71685E-01, 4.62937E-01, 5.16315E-01, 5.83565E-01, 6.27861E-01, 6.74816E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label