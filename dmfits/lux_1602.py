""" lux.py constains fit for results of LUX """

import numpy as np

# Data taken from 1602.03489

@np.vectorize
def sdn_lux_1602(mdm = 100, extrapol=False):
    """ Spin dependent DM-neutron [cm^2] ref: 1602.03489 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label=r"LUX SDn 1602.03489"
    mass_min = 3.56919E+00
    mass_max = 1.02175E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.22151E+01
    fmax = -7.87080E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.59055E-01, -2.22620E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.54472E-03, 6.44024E-03, 1.23728E-02, 1.69570E-02, 2.35517E-02, 3.21569E-02, 3.61128E-02, 4.40623E-02, 5.46901E-02, 7.15702E-02, 9.02609E-02, 1.16119E-01, 1.37547E-01, 1.66609E-01, 1.97029E-01, 2.19866E-01, 2.55709E-01, 2.78126E-01, 2.93353E-01, 3.28319E-01, 3.65082E-01, 4.12604E-01, 4.67755E-01, 5.09004E-01, 5.42185E-01, 5.74023E-01, 6.07651E-01, 6.34106E-01, 6.61456E-01, 6.86568E-01, 7.10331E-01, 7.39479E-01, 7.60107E-01, 7.88807E-01, 8.17052E-01, 8.43058E-01, 8.64136E-01, 8.84757E-01, 9.06731E-01, 9.30497E-01, 9.54712E-01, 9.79819E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.10969E-01, 8.26453E-01, 7.55498E-01, 6.81537E-01, 6.04543E-01, 5.24515E-01, 4.77714E-01, 4.07749E-01, 3.32231E-01, 2.45598E-01, 1.71021E-01, 9.73908E-02, 5.39702E-02, 2.35627E-02, 2.19650E-03, 0.00000E+00, 8.76216E-03, 2.36680E-02, 2.35458E-02, 4.43850E-02, 6.82265E-02, 9.90217E-02, 1.38808E-01, 1.67642E-01, 1.92519E-01, 2.17406E-01, 2.41273E-01, 2.61175E-01, 2.80064E-01, 2.99977E-01, 3.16883E-01, 3.39781E-01, 3.56712E-01, 3.79613E-01, 3.98495E-01, 4.17395E-01, 4.35329E-01, 4.47232E-01, 4.65158E-01, 4.84076E-01, 5.02990E-01, 5.19886E-01, 5.36821E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def sdp_lux_1602(mdm = 100, extrapol=False):
    """ Spin dependent DM-proton [cm^2] ref: 1602.03489 """
    """ extrapol : if True the fitting function return values beyond max and min """
    label=r"LUX SDp 1602.03489"
    mass_min = 3.85340E+00
    mass_max = 1.03513E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -8.87937E+01
    fmax = -7.67071E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [8.17177E-01, -2.35153E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.95797E-03, 7.28831E-03, 1.16187E-02, 1.86049E-02, 2.42914E-02, 3.35102E-02, 4.32304E-02, 5.33928E-02, 6.22717E-02, 7.42810E-02, 8.85578E-02, 1.01966E-01, 1.13132E-01, 1.26581E-01, 1.43179E-01, 1.62042E-01, 1.83599E-01, 2.02044E-01, 2.20948E-01, 2.43022E-01, 2.63743E-01, 2.86734E-01, 3.07023E-01, 3.30923E-01, 3.53030E-01, 3.76485E-01, 3.97685E-01, 4.25203E-01, 4.55890E-01, 4.84311E-01, 5.10928E-01, 5.35295E-01, 5.60561E-01, 5.99358E-01, 6.42675E-01, 6.87673E-01, 7.26585E-01, 7.59071E-01, 7.93584E-01, 8.32843E-01, 8.61942E-01, 8.91040E-01, 9.22849E-01, 9.50256E-01, 9.73944E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.19051E-01, 8.47071E-01, 7.75091E-01, 6.83969E-01, 6.14216E-01, 5.15195E-01, 4.37516E-01, 3.56460E-01, 3.03514E-01, 2.41533E-01, 1.86260E-01, 1.44483E-01, 1.06112E-01, 8.11900E-02, 5.50976E-02, 3.45898E-02, 1.06707E-02, 3.65331E-03, 0.00000E+00, 4.16510E-03, 7.22677E-03, 1.81203E-02, 2.90541E-02, 4.33050E-02, 6.09538E-02, 7.74587E-02, 9.28737E-02, 1.13813E-01, 1.41446E-01, 1.63496E-01, 1.84448E-01, 2.05434E-01, 2.26407E-01, 2.56167E-01, 2.93444E-01, 3.27325E-01, 3.57926E-01, 3.85252E-01, 4.12548E-01, 4.47358E-01, 4.70520E-01, 4.92840E-01, 5.19334E-01, 5.40836E-01, 5.60709E-01, 5.82231E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label