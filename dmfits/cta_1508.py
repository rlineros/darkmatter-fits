""" FITTING FUNCTIONS FOR CTA """

### last modification: 26/feb/2023

import numpy as np

### Data taken from 1508.06128

@np.vectorize
def id_bb_cta_1508(mdm : float = 100, extrapol : bool = False) -> float:
    """ Annihilation cross section into bottom quarks [cm^3 s^-1] ref: 1508.06128 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 5.46030E+01
    mass_max = 5.41138E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -5.94998E+01
    fmax = -5.70224E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.82991E+00, -1.11254E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 4.10529E-03, 1.39016E-02, 2.25333E-02, 3.17407E-02, 4.03716E-02, 4.89989E-02, 5.82076E-02, 6.80107E-02, 7.84234E-02, 8.88362E-02, 9.98290E-02, 1.11409E-01, 1.22981E-01, 1.36126E-01, 1.47669E-01, 1.58868E-01, 1.70706E-01, 1.81769E-01, 1.94265E-01, 2.07307E-01, 2.20178E-01, 2.33056E-01, 2.45930E-01, 2.58810E-01, 2.71738E-01, 2.84695E-01, 2.97645E-01, 3.10601E-01, 3.23552E-01, 3.36530E-01, 3.49532E-01, 3.62529E-01, 3.75523E-01, 3.88522E-01, 4.01545E-01, 4.14604E-01, 4.27658E-01, 4.40706E-01, 4.53758E-01, 4.66819E-01, 4.79895E-01, 4.92972E-01, 5.06043E-01, 5.19116E-01, 5.32207E-01, 5.45313E-01, 5.58421E-01, 5.71528E-01, 5.84634E-01, 5.97747E-01, 6.10876E-01, 6.24005E-01, 6.37138E-01, 6.50266E-01, 6.63398E-01, 6.76538E-01, 6.89276E-01, 7.02350E-01, 7.15976E-01, 7.29073E-01, 7.42169E-01, 7.54980E-01, 7.68523E-01, 7.81666E-01, 7.94808E-01, 8.07949E-01, 8.21087E-01, 8.34229E-01, 8.47375E-01, 8.60512E-01, 8.73667E-01, 8.86831E-01, 8.99993E-01, 9.13159E-01, 9.26320E-01, 9.39494E-01, 9.52722E-01, 9.65868E-01, 9.79021E-01, 9.92233E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.77665E-01, 9.32659E-01, 8.90958E-01, 8.46515E-01, 8.04670E-01, 7.62235E-01, 7.18006E-01, 6.74121E-01, 6.33083E-01, 5.92045E-01, 5.48990E-01, 5.05155E-01, 4.59898E-01, 4.14899E-01, 3.79773E-01, 3.46388E-01, 3.01033E-01, 2.61101E-01, 2.30701E-01, 2.02657E-01, 1.76287E-01, 1.51055E-01, 1.25104E-01, 1.00171E-01, 8.32606E-02, 7.10193E-02, 5.76405E-02, 4.52795E-02, 3.19606E-02, 2.32514E-02, 1.83736E-02, 1.28373E-02, 6.70236E-03, 1.34566E-03, 0.00000E+00, 4.64096E-03, 8.26419E-03, 1.10493E-02, 1.44929E-02, 1.93733E-02, 2.67681E-02, 3.42827E-02, 4.08992E-02, 4.77552E-02, 5.75447E-02, 6.99084E-02, 8.25115E-02, 9.49950E-02, 1.07299E-01, 1.20800E-01, 1.36935E-01, 1.52951E-01, 1.69685E-01, 1.85700E-01, 2.02255E-01, 2.20246E-01, 2.36971E-01, 2.62485E-01, 2.76853E-01, 2.87780E-01, 3.09173E-01, 3.27851E-01, 3.46423E-01, 3.64773E-01, 3.83124E-01, 4.01174E-01, 4.18627E-01, 4.36917E-01, 4.55806E-01, 4.73259E-01, 4.93644E-01, 5.15527E-01, 5.37110E-01, 5.59232E-01, 5.80635E-01, 6.04134E-01, 6.36493E-01, 6.55382E-01, 6.75529E-01, 7.05313E-01, 7.16241E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_bb_cta_1508,"label", r"CTA-GC $b\bar{b}$ 1508.06128")
setattr(id_bb_cta_1508,"url", "https://inspirehep.net/literature/1389681")
setattr(id_bb_cta_1508,"mass_min", 5.46030E+01)
setattr(id_bb_cta_1508,"mass_max", 5.41138E+04)


@np.vectorize
def id_tau_cta_1508(mdm : float = 100, extrapol : bool = False) -> float:
    """ Annihilation cross section into taus [cm^3 s^-1] ref: 1508.06128 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 5.17981E+01
    mass_max = 5.00953E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.10077E+01
    fmax = -5.76970E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.27017E+00, -1.27129E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.28643E-02, 2.57210E-02, 3.85809E-02, 5.14398E-02, 6.43172E-02, 7.72418E-02, 9.01717E-02, 1.03104E-01, 1.16031E-01, 1.28984E-01, 1.42007E-01, 1.55034E-01, 1.68060E-01, 1.81088E-01, 1.94130E-01, 2.07207E-01, 2.20284E-01, 2.33360E-01, 2.46437E-01, 2.59516E-01, 2.72645E-01, 2.85788E-01, 2.98934E-01, 3.12078E-01, 3.25226E-01, 3.38380E-01, 3.51535E-01, 3.64690E-01, 3.77846E-01, 3.91003E-01, 4.04176E-01, 4.17356E-01, 4.30539E-01, 4.43720E-01, 4.56901E-01, 4.70078E-01, 4.83253E-01, 4.96427E-01, 5.09602E-01, 5.22778E-01, 5.35956E-01, 5.49141E-01, 5.62327E-01, 5.75513E-01, 5.88697E-01, 6.01896E-01, 6.15103E-01, 6.28314E-01, 6.41520E-01, 6.52929E-01, 6.67955E-01, 6.81171E-01, 6.94391E-01, 7.07622E-01, 7.20854E-01, 7.34096E-01, 7.47361E-01, 7.60627E-01, 7.73892E-01, 7.87159E-01, 8.00429E-01, 8.13708E-01, 8.26993E-01, 8.39325E-01, 8.53565E-01, 8.66840E-01, 8.80138E-01, 8.93444E-01, 9.06745E-01, 9.20050E-01, 9.33353E-01, 9.46678E-01, 9.60016E-01, 9.73345E-01, 9.86671E-01, 1.00000E+00 ]
        vval = [2.50062E-01, 2.23879E-01, 1.96755E-01, 1.70035E-01, 1.43180E-01, 1.18609E-01, 9.98179E-02, 8.16984E-02, 6.38476E-02, 4.53249E-02, 3.00277E-02, 2.33315E-02, 1.71728E-02, 1.08798E-02, 4.72115E-03, 4.43998E-04, 3.32999E-04, 2.21999E-04, 1.11000E-04, 0.00000E+00, 1.57784E-04, 6.49760E-03, 1.45845E-02, 2.29402E-02, 3.11615E-02, 3.97860E-02, 4.92168E-02, 5.86476E-02, 6.82129E-02, 7.77781E-02, 8.76121E-02, 9.93276E-02, 1.11849E-01, 1.24774E-01, 1.37565E-01, 1.50221E-01, 1.62474E-01, 1.74459E-01, 1.86309E-01, 1.98293E-01, 2.10277E-01, 2.22665E-01, 2.35858E-01, 2.49187E-01, 2.62515E-01, 2.75574E-01, 2.90515E-01, 3.06397E-01, 3.22816E-01, 3.38563E-01, 3.52662E-01, 3.72880E-01, 3.89971E-01, 4.07465E-01, 4.26304E-01, 4.45276E-01, 4.65459E-01, 4.88463E-01, 5.11602E-01, 5.34606E-01, 5.57879E-01, 5.81556E-01, 6.06307E-01, 6.31731E-01, 6.53683E-01, 6.82981E-01, 7.07195E-01, 7.34231E-01, 7.62342E-01, 7.89782E-01, 8.17624E-01, 8.45332E-01, 8.75728E-01, 9.07737E-01, 9.38670E-01, 9.69066E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_tau_cta_1508,"label", r"CTA-GC $\tau^+\tau^-$ 1508.06128")
setattr(id_tau_cta_1508,"url", "https://inspirehep.net/literature/1389681")
setattr(id_tau_cta_1508,"mass_min", 5.17981E+01)
setattr(id_tau_cta_1508,"mass_max", 5.00953E+04)


@np.vectorize
def id_tt_cta_1508(mdm : float = 100, extrapol : bool = False) -> float:
    """ Annihilation cross section into top quarks [cm^3 s^-1] ref: 1508.06128 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 2.21354E+02
    mass_max = 8.27749E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -5.91342E+01
    fmax = -5.72386E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.24350E+00, -1.24513E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.27793E-02, 2.48747E-02, 3.69763E-02, 4.90748E-02, 6.18474E-02, 7.83315E-02, 9.10917E-02, 1.06126E-01, 1.21157E-01, 1.36191E-01, 1.51251E-01, 1.66370E-01, 1.81495E-01, 1.96616E-01, 2.11739E-01, 2.26871E-01, 2.42044E-01, 2.57218E-01, 2.72392E-01, 2.87566E-01, 3.02747E-01, 3.17968E-01, 3.33195E-01, 3.48420E-01, 3.63646E-01, 3.78872E-01, 3.94110E-01, 4.09346E-01, 4.24584E-01, 4.39824E-01, 4.55064E-01, 4.70332E-01, 4.85608E-01, 5.00183E-01, 5.25904E-01, 5.41169E-01, 5.56460E-01, 5.71748E-01, 5.87038E-01, 6.02326E-01, 6.17619E-01, 6.32909E-01, 6.48199E-01, 6.63487E-01, 6.78777E-01, 6.94070E-01, 7.09375E-01, 7.24675E-01, 7.39979E-01, 7.55280E-01, 7.70587E-01, 7.85915E-01, 8.01244E-01, 8.16570E-01, 8.31899E-01, 8.47227E-01, 8.62556E-01, 8.77885E-01, 8.93210E-01, 9.08542E-01, 9.23871E-01, 9.39229E-01, 9.54596E-01, 9.69963E-01, 9.85332E-01, 1.00000E+00 ]
        vval = [5.23202E-01, 4.62846E-01, 4.03589E-01, 3.45479E-01, 2.86796E-01, 2.25207E-01, 1.69652E-01, 1.48284E-01, 1.22270E-01, 9.55528E-02, 6.95393E-02, 4.82202E-02, 3.79332E-02, 2.85851E-02, 1.85328E-02, 8.94996E-03, 1.01020E-03, 5.81604E-04, 3.87734E-04, 1.93867E-04, 0.00000E+00, 9.79754E-04, 9.47068E-03, 1.91352E-02, 2.83303E-02, 3.77602E-02, 4.71900E-02, 5.87323E-02, 7.00399E-02, 8.15823E-02, 9.35941E-02, 1.05606E-01, 1.22782E-01, 1.41366E-01, 1.57893E-01, 1.94841E-01, 2.11313E-01, 2.32713E-01, 2.53645E-01, 2.74811E-01, 2.95742E-01, 3.17612E-01, 3.38778E-01, 3.59944E-01, 3.80876E-01, 4.02042E-01, 4.23912E-01, 4.47895E-01, 4.70938E-01, 4.94687E-01, 5.17965E-01, 5.42417E-01, 5.70625E-01, 5.99067E-01, 6.27041E-01, 6.55483E-01, 6.83691E-01, 7.12133E-01, 7.40576E-01, 7.68314E-01, 7.97226E-01, 8.25668E-01, 8.59510E-01, 8.94994E-01, 9.30478E-01, 9.66197E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_tt_cta_1508,"label", r"CTA-GC $t\bar{t}$ 1508.06128")
setattr(id_tt_cta_1508,"url", "https://inspirehep.net/literature/1389681")
setattr(id_tt_cta_1508,"mass_min", 2.21354E+02)
setattr(id_tt_cta_1508,"mass_max", 8.27749E+04)


@np.vectorize
def id_ww_cta_1508(mdm : float = 100, extrapol : bool = False) -> float:
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: 1508.06128 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 1.35890E+02
    mass_max = 8.61693E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.00638E+01
    fmax = -5.81243E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.14140E+00, -1.13497E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 9.06791E-03, 1.93214E-02, 2.80971E-02, 3.97368E-02, 5.58408E-02, 6.60027E-02, 8.23511E-02, 1.00150E-01, 1.14532E-01, 1.30328E-01, 1.42096E-01, 1.55864E-01, 1.69663E-01, 1.84061E-01, 1.98460E-01, 2.12901E-01, 2.29302E-01, 2.45709E-01, 2.62117E-01, 2.78516E-01, 2.93665E-01, 3.10081E-01, 3.26510E-01, 3.42928E-01, 3.59379E-01, 3.75252E-01, 3.91128E-01, 4.08260E-01, 4.24138E-01, 4.40652E-01, 4.57169E-01, 4.73681E-01, 4.90189E-01, 5.05467E-01, 5.22015E-01, 5.37293E-01, 5.53839E-01, 5.69119E-01, 5.85679E-01, 6.01597E-01, 6.16886E-01, 6.32817E-01, 6.48146E-01, 6.63482E-01, 6.77555E-01, 6.92903E-01, 7.08260E-01, 7.22348E-01, 7.37709E-01, 7.51802E-01, 7.66530E-01, 7.80613E-01, 7.95343E-01, 8.10072E-01, 8.24259E-01, 8.38289E-01, 8.51244E-01, 8.60416E-01, 8.71795E-01, 8.85907E-01, 9.00017E-01, 9.12884E-01, 9.26985E-01, 9.41249E-01, 9.53627E-01, 9.67711E-01, 9.82435E-01, 1.00000E+00 ]
        vval = [9.40730E-01, 8.80846E-01, 8.20595E-01, 7.66376E-01, 6.82062E-01, 5.85665E-01, 5.07382E-01, 4.59071E-01, 3.87777E-01, 3.33864E-01, 2.73082E-01, 2.38745E-01, 2.06256E-01, 1.79822E-01, 1.46651E-01, 1.13817E-01, 8.90570E-02, 7.62158E-02, 6.46364E-02, 5.30570E-02, 3.97952E-02, 2.99153E-02, 2.00182E-02, 1.26445E-02, 3.16794E-03, 0.00000E+00, 7.94383E-03, 1.63924E-02, 2.27088E-02, 3.15058E-02, 4.09550E-02, 5.08249E-02, 5.98536E-02, 6.80412E-02, 8.33957E-02, 9.95742E-02, 1.14929E-01, 1.30687E-01, 1.46462E-01, 1.64743E-01, 1.81519E-01, 1.99049E-01, 2.18360E-01, 2.43808E-01, 2.70518E-01, 2.98087E-01, 3.27320E-01, 3.58236E-01, 3.88749E-01, 4.20506E-01, 4.51859E-01, 4.83625E-01, 5.13212E-01, 5.45482E-01, 5.77416E-01, 6.27298E-01, 6.46455E-01, 6.65015E-01, 6.88934E-01, 7.17731E-01, 7.52870E-01, 7.87588E-01, 8.18790E-01, 8.51910E-01, 8.83766E-01, 9.10096E-01, 9.39683E-01, 9.70691E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_ww_cta_1508,"label", r"CTA-GC $W^+W^-$ 1508.06128")
setattr(id_ww_cta_1508,"url", "https://inspirehep.net/literature/1389681")
setattr(id_ww_cta_1508,"mass_min", 1.35890E+02)
setattr(id_ww_cta_1508,"mass_max", 8.61693E+04)
