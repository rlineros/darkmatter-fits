""" FITTING FUNCTIONS FOR MAGIC FERMI-LAT """

import numpy as np

# Data taken from 1601.06590

@np.vectorize
def id_bb_magicfermi_1601(mdm = 100, extrapol=False):
    """ Annihilation cross section into b-quark [cm^3 s^-1] ref: 1601.06590 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"MAGIC+FERMI-LAT DSph $b\bar{b}$ 1601.06590"
    mass_min = 9.94413E+00
    mass_max = 9.93472E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -6.07332E+01
    fmax = -5.18756E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.00018E+00, -5.79906E-04 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.03310E-02, 5.36824E-02, 7.63761E-02, 9.90698E-02, 1.21763E-01, 1.44457E-01, 1.67151E-01, 1.89844E-01, 2.12538E-01, 2.35232E-01, 2.57925E-01, 2.80619E-01, 3.03313E-01, 3.26006E-01, 3.48700E-01, 3.71394E-01, 3.94087E-01, 4.17141E-01, 4.38913E-01, 4.62168E-01, 4.84862E-01, 5.07556E-01, 5.30249E-01, 5.52943E-01, 5.75637E-01, 5.98330E-01, 6.21024E-01, 6.43718E-01, 6.66411E-01, 6.89105E-01, 7.11799E-01, 7.34492E-01, 7.57186E-01, 7.79880E-01, 8.02573E-01, 8.25267E-01, 8.47961E-01, 8.70654E-01, 8.93348E-01, 9.16042E-01, 9.38735E-01, 9.61429E-01, 9.84123E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 2.37243E-02, 4.15702E-02, 5.60343E-02, 7.04502E-02, 8.53786E-02, 1.02389E-01, 1.17500E-01, 1.32940E-01, 1.49763E-01, 1.67516E-01, 1.86568E-01, 2.06221E-01, 2.26056E-01, 2.46913E-01, 2.68057E-01, 2.89831E-01, 3.13382E-01, 3.35368E-01, 3.57016E-01, 3.79235E-01, 4.04089E-01, 4.23602E-01, 4.40208E-01, 4.59510E-01, 4.79713E-01, 5.00948E-01, 5.23389E-01, 5.49498E-01, 5.76138E-01, 6.05625E-01, 6.37284E-01, 6.71983E-01, 7.07020E-01, 7.42106E-01, 7.76322E-01, 8.09477E-01, 8.38820E-01, 8.67052E-01, 8.91810E-01, 9.16133E-01, 9.38333E-01, 9.59757E-01, 9.84061E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_mu_magicfermi_1601(mdm = 100, extrapol=False):
    """ Annihilation cross section into muons [cm^3 s^-1] ref: 1601.06590 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"MAGIC+FERMI-LAT DSph $\mu^+\mu^-$ 1601.06590"
    mass_min = 9.95090E+00
    mass_max = 1.00370E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.88240E+01
    fmax = -4.86821E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.76922E+00, -7.71427E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.08562E-02, 5.35284E-02, 7.62005E-02, 9.88726E-02, 1.21545E-01, 1.44217E-01, 1.67380E-01, 1.88881E-01, 2.12135E-01, 2.34905E-01, 2.57578E-01, 2.80250E-01, 3.02922E-01, 3.25594E-01, 3.48266E-01, 3.70938E-01, 3.93610E-01, 4.16283E-01, 4.38955E-01, 4.61627E-01, 4.84922E-01, 5.01573E-01, 5.20358E-01, 5.33584E-01, 5.44295E-01, 5.64257E-01, 5.86863E-01, 6.09290E-01, 6.32208E-01, 6.54880E-01, 6.77552E-01, 7.00224E-01, 7.22896E-01, 7.45568E-01, 7.68240E-01, 7.90913E-01, 8.13585E-01, 8.36257E-01, 8.58929E-01, 8.81061E-01, 9.02654E-01, 9.24246E-01, 9.45299E-01, 9.65812E-01, 9.87404E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 2.03297E-02, 3.36251E-02, 4.73900E-02, 6.70645E-02, 8.77755E-02, 1.04973E-01, 1.23073E-01, 1.40380E-01, 1.61789E-01, 1.85588E-01, 2.08237E-01, 2.30506E-01, 2.43172E-01, 2.41407E-01, 2.38435E-01, 2.40038E-01, 2.58132E-01, 2.78546E-01, 3.09250E-01, 3.45564E-01, 3.80091E-01, 4.13657E-01, 4.58540E-01, 4.97477E-01, 5.22692E-01, 5.42436E-01, 5.70181E-01, 5.88941E-01, 5.98467E-01, 5.97486E-01, 5.97162E-01, 6.01830E-01, 6.07988E-01, 6.15580E-01, 6.33126E-01, 6.56745E-01, 6.87955E-01, 7.20938E-01, 7.54932E-01, 7.90856E-01, 8.26639E-01, 8.63617E-01, 9.00160E-01, 9.36521E-01, 9.74040E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_tau_magicfermi_1601(mdm = 100, extrapol=False):
    """ Annihilation cross section into tau-lepton [cm^3 s^-1] ref: 1601.06590 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"MAGIC+FERMI-LAT DSph $\tau^+\tau^-$ 1601.06590"
    mass_min = 9.98303E+00
    mass_max = 1.01135E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -6.07426E+01
    fmax = -5.06360E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.73065E+00, -7.33987E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.08261E-02, 5.34819E-02, 7.61377E-02, 9.87935E-02, 1.21449E-01, 1.44105E-01, 1.66761E-01, 1.89417E-01, 2.12073E-01, 2.34728E-01, 2.57384E-01, 2.80040E-01, 3.02696E-01, 3.25352E-01, 3.48253E-01, 3.69854E-01, 3.93319E-01, 4.15975E-01, 4.38631E-01, 4.61286E-01, 4.83942E-01, 5.07208E-01, 5.28997E-01, 5.46830E-01, 5.59596E-01, 5.78119E-01, 5.97761E-01, 6.13404E-01, 6.35386E-01, 6.57637E-01, 6.80293E-01, 7.02948E-01, 7.25604E-01, 7.48260E-01, 7.70916E-01, 7.93572E-01, 8.16227E-01, 8.38883E-01, 8.61539E-01, 8.84195E-01, 9.06851E-01, 9.28967E-01, 9.50544E-01, 9.71042E-01, 9.91540E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 1.40673E-02, 2.61500E-02, 3.88513E-02, 5.52286E-02, 7.25793E-02, 9.20599E-02, 1.11293E-01, 1.30683E-01, 1.50310E-01, 1.75405E-01, 1.98604E-01, 2.23784E-01, 2.46636E-01, 2.66906E-01, 2.84435E-01, 2.89826E-01, 2.98518E-01, 3.08675E-01, 3.24544E-01, 3.45619E-01, 3.72576E-01, 4.04919E-01, 4.40286E-01, 4.79086E-01, 5.15387E-01, 5.50655E-01, 5.83148E-01, 6.17261E-01, 6.42979E-01, 6.69226E-01, 6.88377E-01, 6.97678E-01, 7.03846E-01, 7.07902E-01, 7.10914E-01, 7.18700E-01, 7.33977E-01, 7.53740E-01, 7.76465E-01, 8.05919E-01, 8.38462E-01, 8.71945E-01, 9.08470E-01, 9.46170E-01, 9.80454E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_ww_magicfermi_1601(mdm =100, extrapol=False):
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: 1601.06590 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"MAGIC+FERMI-LAT DSph $W^+W^-$ 1601.06590"
    mass_min = 9.00506E+01
    mass_max = 1.00812E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.89349E+01
    fmax = -5.15137E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [9.48352E-01, 5.01082E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.22062E-02, 5.48944E-02, 7.75826E-02, 1.00271E-01, 1.22959E-01, 1.45681E-01, 1.67705E-01, 1.91024E-01, 2.13712E-01, 2.36400E-01, 2.59088E-01, 2.81776E-01, 3.04465E-01, 3.27153E-01, 3.49841E-01, 3.72529E-01, 3.95217E-01, 4.17906E-01, 4.40594E-01, 4.63282E-01, 4.85970E-01, 5.08659E-01, 5.31347E-01, 5.54035E-01, 5.76723E-01, 5.99411E-01, 6.22100E-01, 6.44788E-01, 6.67476E-01, 6.90164E-01, 7.12852E-01, 7.35541E-01, 7.58229E-01, 7.78983E-01, 8.02390E-01, 8.27930E-01, 8.48356E-01, 8.71670E-01, 8.94358E-01, 9.17046E-01, 9.39734E-01, 9.62423E-01, 9.85736E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 2.59539E-02, 4.32694E-02, 6.07866E-02, 7.92190E-02, 9.76846E-02, 1.14224E-01, 1.32688E-01, 1.53363E-01, 1.72796E-01, 1.93967E-01, 2.13627E-01, 2.28674E-01, 2.46242E-01, 2.62776E-01, 2.80394E-01, 2.98087E-01, 3.16991E-01, 3.38011E-01, 3.59939E-01, 3.83984E-01, 4.08786E-01, 4.36082E-01, 4.66403E-01, 4.96724E-01, 5.28255E-01, 5.61601E-01, 5.94946E-01, 6.28821E-01, 6.62318E-01, 6.92715E-01, 7.22128E-01, 7.49501E-01, 7.75663E-01, 7.97417E-01, 8.20186E-01, 8.43621E-01, 8.62325E-01, 8.83715E-01, 9.01408E-01, 9.21295E-01, 9.40047E-01, 9.61824E-01, 9.84152E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label