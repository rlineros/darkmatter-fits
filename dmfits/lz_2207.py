""" FITTING FUNCTIONS FOR LUX-ZEPLIN """

### last modification: 22/feb/2023

import numpy as np

# Data taken from 2207.03764

@np.vectorize
def sdn_lz_2207(mdm = 100, extrapol=False):
    """ Spin dependent DM-neutron cross-section [cm^2] ref: 2207.03764 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"LZ SDn 2207.0376"
    mass_min = 9.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.67363E+01
    fmax = -9.05328E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.16737E+00, -1.67367E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.86136E-02, 5.24339E-02, 8.20412E-02, 9.06856E-02, 1.06545E-01, 1.20816E-01, 1.33788E-01, 1.51270E-01, 1.66840E-01, 1.71674E-01, 1.80877E-01, 1.97672E-01, 2.12695E-01, 2.23007E-01, 2.32624E-01, 2.58103E-01, 2.81924E-01, 3.07921E-01, 3.29901E-01, 3.56939E-01, 3.79658E-01, 4.05840E-01, 4.28737E-01, 4.55125E-01, 4.77385E-01, 5.03753E-01, 5.26393E-01, 5.52653E-01, 5.75102E-01, 6.01659E-01, 6.24039E-01, 6.50500E-01, 6.72811E-01, 6.99326E-01, 7.21675E-01, 7.48173E-01, 7.70510E-01, 9.01164E-01, 1.00000E+00 ]
        vval = [6.49527E-01, 4.40761E-01, 2.93477E-01, 1.39595E-01, 1.13345E-01, 6.34948E-02, 3.48830E-02, 2.15598E-02, 1.12568E-02, 0.00000E+00, 6.31974E-03, 1.87975E-02, 4.41173E-02, 6.87131E-02, 8.51062E-02, 1.02307E-01, 1.42113E-01, 1.75152E-01, 2.10944E-01, 2.36711E-01, 2.66298E-01, 2.92949E-01, 3.28078E-01, 3.51814E-01, 3.80246E-01, 4.06671E-01, 4.35663E-01, 4.62359E-01, 4.94177E-01, 5.19363E-01, 5.50850E-01, 5.70787E-01, 6.03527E-01, 6.28659E-01, 6.56297E-01, 6.85710E-01, 7.11995E-01, 7.38345E-01, 8.84622E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def sdp_lz_2207(mdm = 100, extrapol=False):
    """ Spin dependent DM-proton cross-section [cm^2] ref: 2207.03764 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = "LZ SDp 2207.03764"
    mass_min = 9.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.33022E+01
    fmax = -8.70815E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.11539E+00, -1.15386E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.86136E-02, 5.24339E-02, 8.20412E-02, 9.06856E-02, 1.06545E-01, 1.20816E-01, 1.33788E-01, 1.51270E-01, 1.66840E-01, 1.71674E-01, 1.80877E-01, 1.97672E-01, 2.12695E-01, 2.23007E-01, 2.32624E-01, 2.58103E-01, 2.81924E-01, 3.07921E-01, 3.29901E-01, 3.56939E-01, 3.79658E-01, 4.05840E-01, 4.28737E-01, 4.55125E-01, 4.77385E-01, 5.03753E-01, 5.26393E-01, 5.52653E-01, 5.75102E-01, 6.01659E-01, 6.24039E-01, 6.50500E-01, 6.72811E-01, 6.99326E-01, 7.21675E-01, 7.48173E-01, 7.70510E-01, 9.01164E-01, 1.00000E+00 ]
        vval = [6.62255E-01, 4.43659E-01, 2.94906E-01, 1.37984E-01, 1.02307E-01, 6.73057E-02, 3.77791E-02, 1.87787E-02, 4.20644E-03, 0.00000E+00, 1.15495E-02, 2.53470E-02, 6.11096E-02, 8.47573E-02, 1.07701E-01, 1.17732E-01, 1.61637E-01, 1.91514E-01, 2.22920E-01, 2.49779E-01, 2.80274E-01, 3.04116E-01, 3.34910E-01, 3.56619E-01, 3.89876E-01, 4.13154E-01, 4.42162E-01, 4.68519E-01, 4.97772E-01, 5.24411E-01, 5.51770E-01, 5.76006E-01, 6.08276E-01, 6.33021E-01, 6.63438E-01, 6.85979E-01, 7.15760E-01, 7.42249E-01, 8.89760E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def si_lz_2207(mdm = 100, extrapol=False):
    """ Spin independent DM-Nucleon [cm^2] ref: 2207.03764 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"LZ SI 2207.03764"
    mass_min = 9.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.08749E+02
    fmax = -1.02611E+02 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.15581E+00, -1.55812E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.86136E-02, 5.24339E-02, 8.20412E-02, 9.06856E-02, 1.06545E-01, 1.20816E-01, 1.33788E-01, 1.51270E-01, 1.66840E-01, 1.71674E-01, 1.80877E-01, 1.97672E-01, 2.12695E-01, 2.23007E-01, 2.32624E-01, 2.58103E-01, 2.81924E-01, 3.07921E-01, 3.29901E-01, 3.56939E-01, 3.79658E-01, 4.05840E-01, 4.28737E-01, 4.55125E-01, 4.77385E-01, 5.03753E-01, 5.26393E-01, 5.52653E-01, 5.75102E-01, 6.01659E-01, 6.24039E-01, 6.50500E-01, 6.72811E-01, 6.99326E-01, 7.21675E-01, 7.48173E-01, 7.70510E-01, 9.01164E-01, 1.00000E+00 ]
        vval = [6.72501E-01, 4.57488E-01, 3.07800E-01, 1.47467E-01, 1.11124E-01, 7.37433E-02, 5.03605E-02, 2.89782E-02, 7.83740E-03, 2.41207E-04, 0.00000E+00, 1.61886E-02, 3.71000E-02, 7.01217E-02, 8.11376E-02, 9.90373E-02, 1.35599E-01, 1.69529E-01, 2.02456E-01, 2.26720E-01, 2.61249E-01, 2.89039E-01, 3.21995E-01, 3.46742E-01, 3.78411E-01, 4.04461E-01, 4.32726E-01, 4.57993E-01, 4.89784E-01, 5.15982E-01, 5.44475E-01, 5.69539E-01, 5.98446E-01, 6.25775E-01, 6.54001E-01, 6.82934E-01, 7.10871E-01, 7.38238E-01, 8.85764E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label