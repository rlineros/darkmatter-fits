""" FITTING FUNCTIONS FOR CTA """

### last modification: 26/feb/2023

import numpy as np

### Data taken from 1709.07997

@np.vectorize
def id_bb_cta_1709(mdm:float = 100, extrapol:bool = False) -> float :
    """ Annihilation cross section into b-quarks [cm^3 s^-1] ref: 1709.07997 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 4.90703E+01
    mass_max = 7.80256E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -5.95144E+01
    fmax = -5.72573E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.46705E+00, -1.46845E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 6.65078E-03, 2.98975E-02, 5.31493E-02, 7.58413E-02, 9.24358E-02, 1.10692E-01, 1.33913E-01, 1.57129E-01, 1.80340E-01, 2.03530E-01, 2.26709E-01, 2.49883E-01, 2.73044E-01, 2.96210E-01, 3.19358E-01, 3.42504E-01, 3.65650E-01, 3.88788E-01, 4.11922E-01, 4.35054E-01, 4.58182E-01, 4.81306E-01, 5.04430E-01, 5.27547E-01, 5.50664E-01, 5.73777E-01, 5.96886E-01, 6.19995E-01, 6.43104E-01, 6.66212E-01, 6.89318E-01, 7.12426E-01, 7.35530E-01, 7.58634E-01, 7.81734E-01, 8.04835E-01, 8.27933E-01, 8.51029E-01, 8.74125E-01, 8.97214E-01, 9.18574E-01, 9.43390E-01, 9.66474E-01, 9.89009E-01, 1.00000E+00 ]
        vval = [7.56375E-01, 7.21703E-01, 6.29061E-01, 5.31591E-01, 4.42344E-01, 3.85589E-01, 3.20980E-01, 2.52374E-01, 1.87801E-01, 1.28270E-01, 8.80935E-02, 5.86829E-02, 3.28007E-02, 1.99593E-02, 1.44343E-03, 0.00000E+00, 8.37912E-04, 1.80418E-03, 9.05080E-03, 2.12962E-02, 3.41822E-02, 5.21955E-02, 7.25157E-02, 9.36048E-02, 1.20846E-01, 1.48216E-01, 1.79046E-01, 2.14106E-01, 2.49038E-01, 2.84098E-01, 3.19231E-01, 3.57422E-01, 3.92738E-01, 4.31772E-01, 4.71958E-01, 5.14452E-01, 5.57074E-01, 6.02004E-01, 6.48343E-01, 6.95835E-01, 7.48455E-01, 7.99336E-01, 8.57154E-01, 9.15157E-01, 9.71009E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_bb_cta_1709,"label", r"CTA-GC $b\bar{b}$ 1709.07997")
setattr(id_bb_cta_1709,"url", "https://inspirehep.net/literature/1626174")
setattr(id_bb_cta_1709,"mass_min", 4.90703E+01)
setattr(id_bb_cta_1709,"mass_max", 7.80256E+04)

@np.vectorize
def id_tau_cta_1709(mdm:float = 100, extrapol:float = False) -> float:
    """ Annihilation cross section into tau pairs [cm^3 s^-1] ref: 1709.07997 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 4.91461E+01
    mass_max = 7.66882E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.09976E+01
    fmax = -5.69629E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.39839E+00, -1.39915E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.32917E-02, 3.65495E-02, 5.98063E-02, 8.30428E-02, 1.06272E-01, 1.29498E-01, 1.52708E-01, 1.75918E-01, 1.99123E-01, 2.22323E-01, 2.45524E-01, 2.68714E-01, 2.91898E-01, 3.15081E-01, 3.38254E-01, 3.61426E-01, 3.84597E-01, 4.07765E-01, 4.30933E-01, 4.54101E-01, 4.77269E-01, 5.00437E-01, 5.23601E-01, 5.46765E-01, 5.69983E-01, 5.93083E-01, 6.16238E-01, 6.39390E-01, 6.62537E-01, 6.85684E-01, 7.08822E-01, 7.31957E-01, 7.55092E-01, 7.78223E-01, 8.01575E-01, 8.24481E-01, 8.47604E-01, 8.70727E-01, 8.93841E-01, 9.16952E-01, 9.40060E-01, 9.63156E-01, 9.86251E-01, 1.00000E+00 ]
        vval = [1.11633E-01, 9.55221E-02, 6.86729E-02, 4.23973E-02, 2.65900E-02, 1.43677E-02, 4.08146E-03, 2.18410E-03, 0.00000E+00, 3.25448E-04, 3.30373E-03, 5.63676E-03, 1.42795E-02, 2.52883E-02, 3.73010E-02, 5.39025E-02, 7.15795E-02, 8.96150E-02, 1.09228E-01, 1.28841E-01, 1.48239E-01, 1.67923E-01, 1.87608E-01, 2.08942E-01, 2.30778E-01, 2.53052E-01, 2.79325E-01, 3.05247E-01, 3.33321E-01, 3.63474E-01, 3.93842E-01, 4.28942E-01, 4.65548E-01, 5.02441E-01, 5.40840E-01, 5.79631E-01, 6.20218E-01, 6.62919E-01, 7.05834E-01, 7.53196E-01, 8.02493E-01, 8.52722E-01, 9.09548E-01, 9.67162E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_tau_cta_1709,"label", r"CTA-GC $\tau^+\tau^-$ 1709.07997")
setattr(id_tau_cta_1709,"url", "https://inspirehep.net/literature/1626174")
setattr(id_tau_cta_1709,"mass_min", 4.91461E+01)
setattr(id_tau_cta_1709,"mass_max", 7.66882E+04)



@np.vectorize
def id_tt_cta_1709(mdm:float = 200, extrapol:float = False) -> float:
    """ Annihilation cross section into top quarks [cm^3 s^-1] ref: 1709.07997 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 1.95773E+02
    mass_max = 7.70469E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -5.91556E+01
    fmax = -5.71339E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.09262E+00, -1.09207E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.36537E-02, 4.25181E-02, 7.09706E-02, 9.95767E-02, 1.28161E-01, 1.56742E-01, 1.85309E-01, 2.13873E-01, 2.42430E-01, 2.70975E-01, 2.99519E-01, 3.28057E-01, 3.56591E-01, 3.85124E-01, 4.13649E-01, 4.42174E-01, 4.70695E-01, 4.99209E-01, 5.27652E-01, 5.56237E-01, 5.84751E-01, 6.13265E-01, 6.41771E-01, 6.70275E-01, 6.98778E-01, 7.27282E-01, 7.55786E-01, 7.84288E-01, 8.12787E-01, 8.41285E-01, 8.69777E-01, 8.98266E-01, 9.26754E-01, 9.55238E-01, 9.83722E-01, 1.00000E+00 ]
        vval = [3.24127E-01, 2.77419E-01, 1.89600E-01, 1.05142E-01, 6.28632E-02, 3.89001E-02, 1.75128E-02, 7.71611E-03, 1.06761E-03, 0.00000E+00, 9.09198E-03, 1.83271E-02, 3.34294E-02, 5.15365E-02, 7.05023E-02, 9.57643E-02, 1.22028E-01, 1.50867E-01, 1.85430E-01, 2.19901E-01, 2.55129E-01, 2.89979E-01, 3.25114E-01, 3.66403E-01, 4.09695E-01, 4.53703E-01, 4.96852E-01, 5.40287E-01, 5.85582E-01, 6.33024E-01, 6.80896E-01, 7.34634E-01, 7.90662E-01, 8.47118E-01, 9.06867E-01, 9.67187E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_tt_cta_1709,"label", r"CTA-GC $t\bar{t}$ 1709.07997")
setattr(id_tt_cta_1709,"url", "https://inspirehep.net/literature/1626174")
setattr(id_tt_cta_1709,"mass_min",1.95773E+02)
setattr(id_tt_cta_1709,"mass_max", 7.70469E+04)


@np.vectorize
def id_ww_cta_1709(mdm:float = 100, extrapol:bool = False) -> float:
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: 1709.07997 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 7.73858E+01
    mass_max = 7.27652E+04
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.00127E+01
    fmax = -5.68110E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.69112E+00, -1.19248E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 7.18309E-03, 2.39941E-02, 3.95026E-02, 5.62574E-02, 7.28419E-02, 9.33079E-02, 1.15106E-01, 1.28867E-01, 1.47427E-01, 1.73478E-01, 1.94206E-01, 2.11835E-01, 2.29832E-01, 2.54816E-01, 2.76221E-01, 3.03290E-01, 3.33007E-01, 3.64161E-01, 3.95034E-01, 4.25908E-01, 4.56783E-01, 4.87644E-01, 5.18488E-01, 5.49334E-01, 5.80179E-01, 6.11022E-01, 6.42123E-01, 6.71479E-01, 7.01704E-01, 7.31934E-01, 7.62158E-01, 7.90907E-01, 8.20422E-01, 8.47418E-01, 8.76053E-01, 9.03917E-01, 9.27328E-01, 9.50389E-01, 9.74448E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.62244E-01, 8.76275E-01, 7.94998E-01, 7.10443E-01, 6.30920E-01, 5.52973E-01, 4.68650E-01, 4.16833E-01, 3.60511E-01, 2.94448E-01, 2.41166E-01, 2.03055E-01, 1.68058E-01, 1.30747E-01, 1.04840E-01, 7.64128E-02, 4.83230E-02, 3.41029E-02, 2.41935E-02, 1.39679E-02, 2.31920E-03, 0.00000E+00, 7.80097E-03, 1.41788E-02, 2.15054E-02, 2.97807E-02, 4.78674E-02, 7.17575E-02, 9.51743E-02, 1.15474E-01, 1.39388E-01, 1.71630E-01, 2.10389E-01, 2.54310E-01, 3.00514E-01, 3.43549E-01, 3.74869E-01, 4.02888E-01, 4.53129E-01, 5.06278E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_tt_cta_1709,"label", r"CTA-GC $W^+W^-$ 1709.07997")
setattr(id_tt_cta_1709,"url", "https://inspirehep.net/literature/1626174")
setattr(id_tt_cta_1709,"mass_min", 7.73858E+01)
setattr(id_tt_cta_1709,"mass_max", 7.27652E+04)
