""" Fit for constrain curves of CMB bounds to Dark Matter """

### last modification: 13/jun/2023

import numpy as np

# Data taken from 2105.08334
@np.vectorize
def id_ww_cmb_kawasaki_2105(mdm : float = 200, extrapol : bool = False) -> float:
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: 2105.08334 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 8.00762E+01
    mass_max = 9.90966E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -5.72582E+01
    fmax = -5.24647E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [9.85949E-01, 1.40671E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.09655E-01, 2.36843E-01, 3.59646E-01, 4.97071E-01, 6.77626E-01, 8.45027E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 1.04877E-01, 2.36102E-01, 3.62694E-01, 5.01602E-01, 6.82157E-01, 8.47251E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_ww_cmb_kawasaki_2105,"label", r"Kawasaki et al. $W^+W^-$ 2105.08334")
setattr(id_ww_cmb_kawasaki_2105,"url", "https://inspirehep.net/literature/1863746")
setattr(id_ww_cmb_kawasaki_2105,"mass_min", 8.00762E+01)
setattr(id_ww_cmb_kawasaki_2105,"mass_max", 9.90966E+03)



@np.vectorize
def id_mu_cmb_kawasaki_2105(mdm : float = 200, extrapol : bool = False) -> float:
    """ Annihilation cross section into muons [cm^3 s^-1] ref: 2105.08334 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 1.00124E+00
    mass_max = 9.94235E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.14429E+01
    fmax = -5.22624E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.02162E+00, -1.94433E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 8.72666E-02, 1.63816E-01, 2.47251E-01, 3.27620E-01, 4.14106E-01, 5.07477E-01, 6.05441E-01, 6.74323E-01, 7.49331E-01, 8.26636E-01, 9.12362E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 7.80439E-02, 1.46432E-01, 2.25342E-01, 3.04293E-01, 3.94591E-01, 4.95413E-01, 5.99439E-01, 6.72009E-01, 7.48580E-01, 8.26754E-01, 9.11349E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_mu_cmb_kawasaki_2105,"label", r"Kawasaki et al. $\mu^+\mu^-$ 2105.08334")
setattr(id_mu_cmb_kawasaki_2105,"url", "https://inspirehep.net/literature/1863746")
setattr(id_mu_cmb_kawasaki_2105,"mass_min", 1.00124E+00)
setattr(id_mu_cmb_kawasaki_2105,"mass_max", 9.94235E+03)

@np.vectorize
def id_ee_cmb_kawasaki_2105(mdm : float = 200, extrapol : bool = False) -> float:
    """ Annihilation cross section into electrons [cm^3 s^-1] ref: 2105.08334 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 9.98839E-01
    mass_max = 9.84993E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.24246E+01
    fmax = -5.33415E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.00974E+00, -8.71991E-03 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 9.00296E-02, 1.79665E-01, 2.57794E-01, 3.22895E-01, 3.84932E-01, 4.50798E-01, 5.01348E-01, 5.66452E-01, 6.30795E-01, 7.07392E-01, 7.83991E-01, 8.52930E-01, 9.21103E-01, 9.60551E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 6.72938E-02, 1.44494E-01, 2.23495E-01, 2.96068E-01, 3.65381E-01, 4.39594E-01, 4.95032E-01, 5.64304E-01, 6.28636E-01, 7.06833E-01, 7.84205E-01, 8.52602E-01, 9.21009E-01, 9.60917E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_ee_cmb_kawasaki_2105,"label", r"Kawasaki et al. $e^+e^-$ 2105.08334")
setattr(id_ee_cmb_kawasaki_2105,"url", "https://inspirehep.net/literature/1863746")
setattr(id_ee_cmb_kawasaki_2105,"mass_min", 9.98839E-01)
setattr(id_ee_cmb_kawasaki_2105,"mass_max", 9.84993E+03)


@np.vectorize
def id_2phot_cmb_kawasaki_2105(mdm :float = 200, extrapol : bool = False) -> float:
    """ Annihilation cross section into 2 photons [cm^3 s^-1] ref: 2105.08334 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 1.00930E+00
    mass_max = 9.88464E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.22823E+01
    fmax = -5.33340E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.01856E+00, -1.77865E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 4.98315E-02, 1.11161E-01, 1.70189E-01, 2.17719E-01, 2.73680E-01, 3.45740E-01, 4.07833E-01, 4.57658E-01, 5.25115E-01, 5.98705E-01, 6.76893E-01, 7.43583E-01, 8.14873E-01, 8.93447E-01, 9.55922E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 4.62335E-02, 1.04876E-01, 1.61874E-01, 2.08138E-01, 2.62665E-01, 3.33726E-01, 3.95708E-01, 4.47804E-01, 5.16414E-01, 5.91642E-01, 6.71834E-01, 7.40455E-01, 8.13201E-01, 8.92550E-01, 9.55365E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_ee_cmb_kawasaki_2105,"label", r"Kawasaki et al. $\gamma \gamma$ 2105.08334")
setattr(id_ee_cmb_kawasaki_2105,"url", "https://inspirehep.net/literature/1863746")
setattr(id_ee_cmb_kawasaki_2105,"mass_min", 1.00930E+00)
setattr(id_ee_cmb_kawasaki_2105,"mass_max", 9.88464E+03)