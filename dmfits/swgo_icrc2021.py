""" FITTING FUNCTIONS FOR SWGO """

import numpy as np


# Data taken from ICRC2021_555 https://doi.org/10.22323/1.395.0555

@np.vectorize
def id_bb_swgo_icrc2021(mdm = 1000, extrapol=False):
    """ Annihilation cross section into b-quarks [cm^3 s^-1] ref: ICRC2021_555 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"SWGO-GC $b \overline{b}$ ICRC2021"
    mass_min = 5.01954E+02
    mass_max = 9.91062E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.95775E+01
    fmax = -5.82971E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.38862E+00, -1.39025E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.96849E-02, 7.16363E-02, 1.00948E-01, 1.33397E-01, 1.66711E-01, 1.99299E-01, 2.32504E-01, 2.65707E-01, 2.98908E-01, 3.32105E-01, 3.65300E-01, 3.98492E-01, 4.31682E-01, 4.64870E-01, 4.98053E-01, 5.31239E-01, 5.64421E-01, 5.97601E-01, 6.30778E-01, 6.63955E-01, 6.97130E-01, 7.30241E-01, 7.63477E-01, 7.96649E-01, 8.29819E-01, 8.62988E-01, 8.96157E-01, 9.29327E-01, 9.64625E-01, 1.00000E+00 ]
        vval = [6.46783E-01, 5.12227E-01, 4.18259E-01, 3.33526E-01, 2.64673E-01, 2.03429E-01, 1.39404E-01, 9.24773E-02, 5.67024E-02, 2.73347E-02, 9.11816E-03, 9.34721E-04, 0.00000E+00, 6.30516E-03, 2.23745E-02, 5.51570E-02, 7.54053E-02, 1.11818E-01, 1.54081E-01, 2.03312E-01, 2.57282E-01, 3.15712E-01, 3.84997E-01, 4.47239E-01, 5.16366E-01, 5.92478E-01, 6.75032E-01, 7.52971E-01, 8.31192E-01, 9.10611E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_tau_swgo_icrc2021(mdm = 1000, extrapol=False):
    """ Annihilation cross section into tau [cm^3 s^-1] ref: ICRC2021_555 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"SWGO-GC $\tau^+ \tau^-$ ICRC2021"
    mass_min = 4.91662E+02
    mass_max = 9.55209E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -6.10782E+01
    fmax = -5.90690E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.96052E+00, -9.64754E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 4.65513E-02, 7.96027E-02, 1.12620E-01, 1.45608E-01, 1.78562E-01, 2.11498E-01, 2.44392E-01, 2.77274E-01, 3.10149E-01, 3.42994E-01, 3.75819E-01, 4.08628E-01, 4.41418E-01, 4.74206E-01, 5.06961E-01, 5.39707E-01, 5.72442E-01, 6.05187E-01, 6.37945E-01, 6.70703E-01, 7.03486E-01, 7.36325E-01, 7.69178E-01, 8.02019E-01, 8.34855E-01, 8.67666E-01, 9.00437E-01, 9.33172E-01, 9.65874E-01, 1.00000E+00 ]
        vval = [9.30555E-02, 4.33295E-02, 2.07976E-02, 6.65162E-03, 0.00000E+00, 1.37811E-03, 7.21675E-03, 2.35843E-02, 4.29864E-02, 6.41722E-02, 9.26729E-02, 1.25992E-01, 1.63416E-01, 2.05479E-01, 2.48078E-01, 2.98885E-01, 3.51832E-01, 4.07814E-01, 4.60583E-01, 5.10855E-01, 5.60591E-01, 6.04440E-01, 6.34548E-01, 6.61083E-01, 6.90657E-01, 7.21120E-01, 7.58009E-01, 8.04892E-01, 8.60516E-01, 9.24528E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_ww_swgo_icrc2021(mdm = 500, extrapol=False):
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: ICRC2021_555 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"SWGO-GC $W^+ W^-$ ICRC2021"
    mass_min = 4.97976E+02
    mass_max = 9.84438E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -6.00906E+01
    fmax = -5.82579E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [6.57554E+00, -5.59483E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 4.32631E-02, 7.62081E-02, 1.09125E-01, 1.42015E-01, 1.74156E-01, 2.07818E-01, 2.39985E-01, 2.73549E-01, 3.10514E-01, 3.43353E-01, 3.76175E-01, 4.08991E-01, 4.41817E-01, 4.74620E-01, 5.07428E-01, 5.40293E-01, 5.73128E-01, 6.05964E-01, 6.38820E-01, 6.71694E-01, 7.04566E-01, 7.37428E-01, 7.70270E-01, 8.03086E-01, 8.35874E-01, 8.68612E-01, 8.97208E-01, 9.19646E-01, 9.40023E-01, 9.59190E-01, 9.72796E-01, 9.81388E-01, 9.89043E-01, 1.00000E+00 ]
        vval = [3.31917E-01, 2.30647E-01, 1.65407E-01, 1.19697E-01, 9.35183E-02, 6.38636E-02, 2.38452E-02, 5.98160E-03, 4.71017E-03, 0.00000E+00, 8.65463E-03, 2.95911E-02, 5.51588E-02, 7.34780E-02, 1.07301E-01, 1.37902E-01, 1.29040E-01, 1.40112E-01, 1.51988E-01, 1.48356E-01, 1.33657E-01, 1.19156E-01, 1.12508E-01, 1.19149E-01, 1.44517E-01, 1.88809E-01, 2.68741E-01, 3.72640E-01, 4.76065E-01, 5.84574E-01, 6.78480E-01, 7.83473E-01, 8.53338E-01, 9.24677E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label