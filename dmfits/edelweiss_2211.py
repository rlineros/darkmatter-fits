""" FITTING FUNCTIONS FOR EDELWEISS """

# 26/feb/2023

import numpy as np

### Data taken from 2211.04176

@np.vectorize
def si_edelweiss_2211(mdm = 0.8, extrapol=False):
    """ Spin independent DM-Nucleon [cm^2] ref: 2211.04176 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"EDELWEISS CRYOSEL 2211.04176"
    mass_min = 6.95676E-02
    mass_max = 1.00000E+01
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.36442E+01
    fmax = -7.77757E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [3.17671E-01, -2.53000E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.32802E-03, 5.31209E-03, 9.29615E-03, 1.30589E-02, 1.70429E-02, 2.10270E-02, 2.50111E-02, 2.95183E-02, 3.58566E-02, 4.11687E-02, 4.64807E-02, 5.18666E-02, 5.89762E-02, 6.58578E-02, 7.83533E-02, 9.89376E-02, 1.22842E-01, 1.50730E-01, 1.78619E-01, 2.06507E-01, 2.34396E-01, 2.62284E-01, 2.90173E-01, 3.18061E-01, 3.45950E-01, 3.73838E-01, 4.01726E-01, 4.29615E-01, 4.57503E-01, 4.85392E-01, 5.13280E-01, 5.41169E-01, 5.69057E-01, 5.96946E-01, 6.24834E-01, 6.52722E-01, 6.80611E-01, 7.08499E-01, 7.36388E-01, 7.64276E-01, 7.92165E-01, 8.20053E-01, 8.47942E-01, 8.75830E-01, 9.03718E-01, 9.31607E-01, 9.59495E-01, 9.86056E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.80248E-01, 9.32994E-01, 8.91990E-01, 8.49652E-01, 8.05426E-01, 7.60977E-01, 7.17195E-01, 6.71060E-01, 6.27840E-01, 5.85961E-01, 5.44707E-01, 5.01619E-01, 4.58676E-01, 4.15944E-01, 3.72190E-01, 3.27403E-01, 2.90266E-01, 2.65835E-01, 2.45547E-01, 2.24021E-01, 2.06543E-01, 1.90399E-01, 1.74064E-01, 1.57872E-01, 1.41442E-01, 1.25155E-01, 1.09201E-01, 9.51519E-02, 8.34365E-02, 7.31022E-02, 6.36251E-02, 5.40528E-02, 4.34327E-02, 3.29079E-02, 2.23355E-02, 1.16202E-02, 2.85741E-03, 1.85732E-03, 0.00000E+00, 1.14297E-03, 6.47681E-03, 1.21916E-02, 1.80017E-02, 2.54310E-02, 3.41461E-02, 4.29565E-02, 5.17192E-02, 6.00157E-02, 6.49109E-02 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label