""" FITTING FUNCTIONS FOR PICO """

import numpy as np

# Data taken from 1702.07666

@np.vectorize
def sdp_pico60_1702(mdm = 100, extrapol=False):
    """ Spin dependent DM-proton [cm^2] ref: 1702.07666 """

    label=r"PICO60 SDp 1702.07666"
    mass_min = 4.32156E+00
    mass_max = 1.01135E+03
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.32352E+01
    fmax = -8.63027E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.16674E-01, -3.75797E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 7.56469E-03, 1.57138E-02, 3.23770E-02, 5.15038E-02, 7.24503E-02, 1.00157E-01, 1.27486E-01, 1.59370E-01, 2.00232E-01, 2.36247E-01, 2.67912E-01, 3.00174E-01, 3.40266E-01, 3.92717E-01, 4.25112E-01, 4.56969E-01, 4.92771E-01, 5.37545E-01, 5.73367E-01, 6.17603E-01, 6.50111E-01, 6.85959E-01, 7.19031E-01, 7.50988E-01, 7.90794E-01, 8.32294E-01, 8.70991E-01, 9.04090E-01, 9.37747E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 8.90178E-01, 7.83971E-01, 6.21401E-01, 4.90509E-01, 3.79522E-01, 2.77468E-01, 1.99896E-01, 1.34923E-01, 7.70308E-02, 4.27966E-02, 2.40571E-02, 1.07426E-02, 0.00000E+00, 7.98562E-04, 5.61118E-03, 1.31557E-02, 2.60576E-02, 4.51318E-02, 6.07538E-02, 8.25595E-02, 1.02780E-01, 1.22026E-01, 1.43143E-01, 1.64279E-01, 1.90701E-01, 2.19810E-01, 2.47162E-01, 2.71901E-01, 2.96630E-01, 3.40747E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def si_pico60_1702(mdm = 100, extrapol=False):
    """ Spin independent DM-nucleon [cm^2] ref: 1702.07666 """

    label=r"PICO60 SI 1702.07666"
    mass_min = 3.59408E+00
    mass_max = 1.00231E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.93259E+01
    fmax = -8.87319E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.79459E-01, -1.39055E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 6.52458E-03, 3.33082E-02, 6.00326E-02, 8.91422E-02, 1.09759E-01, 1.52909E-01, 1.97041E-01, 2.54741E-01, 3.28630E-01, 3.97821E-01, 5.06329E-01, 5.70427E-01, 6.62923E-01, 7.41925E-01, 7.97351E-01, 8.44506E-01, 9.07844E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.11046E-01, 6.88578E-01, 5.39485E-01, 4.30007E-01, 3.66115E-01, 2.71531E-01, 1.98364E-01, 1.32777E-01, 7.64029E-02, 3.95576E-02, 1.21184E-02, 1.28429E-03, 0.00000E+00, 3.80160E-03, 5.03552E-03, 1.21184E-02, 2.38660E-02, 4.04042E-02 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label