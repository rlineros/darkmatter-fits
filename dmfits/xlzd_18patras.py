""" FITTING FUNCTIONS FOR XLZD """

import numpy as np

# Data taken from https://agenda.infn.it/event/34455/contributions/202563/

@np.vectorize
def si_xlzd_18patras(mdm = 100, extrapol=False):
    """ Spin independent DM-Nucleon [cm^2] ref: 18th Patras Workshop """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"XLZD SI 18 Patras"
    mass_min = 3.78473E+00
    mass_max = 9.55482E+03
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.12032E+02
    fmax = -1.01366E+02 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [7.30084E-01, -2.73287E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 8.18885E-03, 1.47090E-02, 1.97073E-02, 2.52579E-02, 3.15061E-02, 3.81251E-02, 4.51882E-02, 5.30436E-02, 6.03527E-02, 6.62560E-02, 7.12431E-02, 7.87247E-02, 8.65061E-02, 9.50038E-02, 1.02341E-01, 1.10998E-01, 1.22094E-01, 1.37505E-01, 1.55244E-01, 1.71320E-01, 2.00675E-01, 2.26295E-01, 2.51926E-01, 2.70807E-01, 2.91963E-01, 3.19465E-01, 3.48610E-01, 3.74349E-01, 4.00393E-01, 4.29118E-01, 4.56794E-01, 4.79853E-01, 5.03782E-01, 5.30799E-01, 5.62416E-01, 5.89448E-01, 6.17977E-01, 6.43489E-01, 6.70788E-01, 6.97946E-01, 7.26634E-01, 7.45767E-01, 7.67777E-01, 7.95627E-01, 8.24237E-01, 8.49232E-01, 8.76926E-01, 9.01837E-01, 9.29990E-01, 9.61404E-01, 9.85579E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.31233E-01, 8.92031E-01, 8.52633E-01, 8.12480E-01, 7.70046E-01, 7.26366E-01, 6.79092E-01, 6.30830E-01, 5.78915E-01, 5.39859E-01, 5.02314E-01, 4.59594E-01, 4.08148E-01, 3.61777E-01, 3.19866E-01, 2.79993E-01, 2.29692E-01, 1.85062E-01, 1.33483E-01, 8.74901E-02, 4.10431E-02, 1.92359E-02, 5.63408E-03, 1.64350E-03, 0.00000E+00, 3.55032E-03, 1.33000E-02, 2.44694E-02, 3.69521E-02, 5.29885E-02, 7.07761E-02, 8.53089E-02, 1.01002E-01, 1.19506E-01, 1.41635E-01, 1.59192E-01, 1.79589E-01, 1.97384E-01, 2.17013E-01, 2.37842E-01, 2.58250E-01, 2.72440E-01, 2.87793E-01, 3.07458E-01, 3.28312E-01, 3.46603E-01, 3.66326E-01, 3.85054E-01, 4.05544E-01, 4.29247E-01, 4.45784E-01, 4.56870E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label