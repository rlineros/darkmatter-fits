""" FITTING FUNCTIONS FOR PANDAX """

import numpy as np

# Data taken from 2107.13438
@np.vectorize
def si_pandax4t_2107(mdm : float = 100, extrapol : bool = False) -> tuple[float, float, float, str, str]:
    """ Spin independent DM-Nucleon [cm^2] ref: 2107.13438 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"PandaX-4T SI 2107.13438"
    inspirehep_url = r"https://inspirehep.net/literature/1894664"
    mass_min = 5.00737E+00
    mass_max = 9.95776E+03
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label, inspirehep_url

    fmin = -1.06871E+02
    fmax = -1.00448E+02 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.16363E+00, -4.14810E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label, inspirehep_url
    else:
        # Data for interpolation
        uval = [0.00000E+00, 9.12396E-03, 1.56635E-02, 2.14724E-02, 2.91332E-02, 3.84222E-02, 4.87937E-02, 5.66220E-02, 6.74281E-02, 7.81166E-02, 8.88052E-02, 1.01530E-01, 1.15272E-01, 1.29015E-01, 1.44284E-01, 1.62098E-01, 1.82255E-01, 2.09578E-01, 2.35843E-01, 2.82217E-01, 3.27004E-01, 3.53798E-01, 3.76888E-01, 4.08444E-01, 4.40001E-01, 4.71558E-01, 5.03115E-01, 5.34671E-01, 5.66228E-01, 5.97785E-01, 6.29341E-01, 6.60898E-01, 6.92455E-01, 7.24012E-01, 7.55568E-01, 7.87125E-01, 8.18682E-01, 8.50239E-01, 8.81795E-01, 9.13352E-01, 9.44909E-01, 9.76466E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.18402E-01, 8.62164E-01, 8.10245E-01, 7.51736E-01, 6.89757E-01, 6.23170E-01, 5.70434E-01, 5.04961E-01, 4.44451E-01, 3.90473E-01, 3.35233E-01, 2.80160E-01, 2.25849E-01, 1.67857E-01, 1.13111E-01, 5.42341E-02, 2.77481E-02, 3.79559E-03, 0.00000E+00, 4.71946E-03, 3.40199E-02, 5.97352E-02, 9.08009E-02, 1.17623E-01, 1.50132E-01, 1.84662E-01, 2.15237E-01, 2.43185E-01, 2.77888E-01, 3.15883E-01, 3.53820E-01, 3.91613E-01, 4.28482E-01, 4.65034E-01, 5.01556E-01, 5.38252E-01, 5.74745E-01, 6.11268E-01, 6.47906E-01, 6.84486E-01, 7.20951E-01, 7.48547E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label, inspirehep_url