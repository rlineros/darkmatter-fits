""" FITTING FUNCTIONS FOR XENON1T """

import numpy as np

# Data taken from 1805.12562

@np.vectorize
def si_xenon1t_1805(mdm = 100, extrapol=False):
    """ Spin independent DM-Nuclean [cm^2] ref: 1805.12562 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"XENON1T SI 1805.12562"
    mass_min = 6.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.06823E+02
    fmax = -1.00401E+02 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [4.13607E-01, 4.13607E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.87786E-02, 5.46553E-02, 6.88576E-02, 1.23513E-01, 1.62292E-01, 2.16947E-01, 2.55725E-01, 2.85804E-01, 3.31160E-01, 3.79238E-01, 4.33894E-01, 4.72672E-01, 5.66106E-01, 6.89619E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 6.11135E-01, 4.89709E-01, 4.03097E-01, 1.65211E-01, 5.21243E-02, 0.00000E+00, 1.57142E-02, 4.07103E-02, 8.04716E-02, 1.26415E-01, 1.82812E-01, 2.25217E-01, 3.28568E-01, 4.69198E-01, 8.27214E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label
