""" FITTING FUNCTIONS FOR PICASSO """

import numpy as np

# Data taken from 1611.01499

@np.vectorize
def sdp_picassofinal_1611(mdm = 10, extrapol=False):
    """ Spin dependent DM-proton [cm^2] ref: 1611.01499 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label=r"PICASSO SDp 1611.01499"
    mass_min = 2.45941E+00
    mass_max = 9.92463E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -8.72373E+01
    fmax = -8.40991E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.21701E+00, -1.22238E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 1.33507E-02, 3.42228E-02, 5.27741E-02, 7.88561E-02, 1.13609E-01, 1.54136E-01, 1.98135E-01, 2.49647E-01, 2.97090E-01, 3.53777E-01, 3.97730E-01, 4.48611E-01, 4.97176E-01, 5.41111E-01, 5.88511E-01, 6.38797E-01, 6.86769E-01, 7.31849E-01, 7.73463E-01, 8.10452E-01, 8.45128E-01, 8.79227E-01, 9.20255E-01, 9.60127E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 8.77685E-01, 7.48500E-01, 6.38125E-01, 4.97288E-01, 3.68487E-01, 2.63439E-01, 1.53768E-01, 6.08206E-02, 1.73062E-02, 0.00000E+00, 8.29396E-03, 4.50914E-02, 9.12621E-02, 1.44383E-01, 2.11755E-01, 2.93363E-01, 3.74907E-01, 4.58730E-01, 5.30661E-01, 5.97745E-01, 6.62406E-01, 7.27051E-01, 8.17840E-01, 9.06238E-01, 9.94636E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def si_picassofinal_1611(mdm = 10, extrapol=False):
    """ Spin independent DM-Nucleon [cm^2] ref: 1611.01499 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label=r"PICASSO SI 1611.01499"
    mass_min = 2.21359E+00
    mass_max = 2.49838E+01
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.35293E+01
    fmax = -8.98001E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [-3.33312E-03, 4.33364E-03 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 5.20688E-02, 1.22303E-01, 1.99023E-01, 2.79921E-01, 3.56777E-01, 4.22409E-01, 4.83528E-01, 5.36094E-01, 5.85641E-01, 6.27242E-01, 6.65030E-01, 7.06669E-01, 7.52355E-01, 7.98740E-01, 8.35684E-01, 8.66353E-01, 8.92686E-01, 9.15607E-01, 9.38316E-01, 9.61251E-01, 9.80968E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 8.68471E-01, 6.97726E-01, 5.53354E-01, 4.03429E-01, 3.15349E-01, 2.57370E-01, 2.01052E-01, 1.57155E-01, 1.16710E-01, 9.23012E-02, 7.32080E-02, 5.02777E-02, 3.40674E-02, 2.28970E-02, 1.55452E-02, 8.39529E-03, 8.47901E-03, 1.60430E-03, 1.74305E-03, 0.00000E+00, 1.93801E-04, 2.14097E-03 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

