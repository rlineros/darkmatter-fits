""" FITTING FUNCTIONS FOR XENON-NT """

import numpy as np

# Data taken from 2303.14729
@np.vectorize
def sdn_xenonnt_2303(mdm = 100, extrapol=False):
    """ Spin dependent DM-neutron cross-section [cm^2] ref: 2303.14729 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"Xenon-nT SDn 2303.14729"
    mass_min = 6.00000E+00
    mass_max = 5.00000E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.52442E+01
    fmax = -8.83192E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [6.13836E-01, -2.60724E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.48533E-02, 6.50445E-02, 9.16751E-02, 1.15497E-01, 1.37046E-01, 1.56720E-01, 1.74817E-01, 2.07172E-01, 2.35471E-01, 2.72217E-01, 3.03817E-01, 3.22669E-01, 3.31537E-01, 3.49420E-01, 3.63892E-01, 3.98745E-01, 4.28936E-01, 4.55567E-01, 4.79389E-01, 5.00938E-01, 5.20611E-01, 5.55465E-01, 5.71064E-01, 5.85656E-01, 6.12286E-01, 6.36108E-01, 6.86561E-01, 7.27783E-01, 7.92828E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 7.96894E-01, 6.43678E-01, 5.24891E-01, 4.30938E-01, 3.55399E-01, 2.93883E-01, 2.43309E-01, 1.66181E-01, 1.11609E-01, 5.67483E-02, 2.21504E-02, 7.48213E-03, 3.10490E-03, 0.00000E+00, 5.66493E-04, 7.45043E-03, 1.75824E-02, 2.85919E-02, 3.97877E-02, 5.08130E-02, 6.14710E-02, 8.12735E-02, 9.03934E-02, 9.90386E-02, 1.15067E-01, 1.29587E-01, 1.60800E-01, 1.86450E-01, 2.25610E-01, 3.53130E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def sdp_xenonnT_2303(mdm = 100, extrapol=False):
    """ Spin dependent DM-proton cross-section [cm^2] ref: 2303.14729 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = "Xenon-nT SDp 2303.14729"
    mass_min = 6.00000E+00
    mass_max = 5.00000E+02
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -9.17952E+01
    fmax = -8.48874E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [5.98329E-01, -2.55346E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.48533E-02, 6.50445E-02, 9.16751E-02, 1.15497E-01, 1.37046E-01, 1.56720E-01, 1.74817E-01, 2.07172E-01, 2.35471E-01, 2.72217E-01, 3.03817E-01, 3.22669E-01, 3.31537E-01, 3.49420E-01, 3.63892E-01, 3.98745E-01, 4.28936E-01, 4.55567E-01, 4.79389E-01, 5.00938E-01, 5.20611E-01, 5.55465E-01, 5.71064E-01, 5.85656E-01, 6.12286E-01, 6.36108E-01, 6.86561E-01, 7.27783E-01, 7.92828E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 7.97494E-01, 6.44404E-01, 5.25504E-01, 4.31317E-01, 3.55482E-01, 2.93554E-01, 2.42559E-01, 1.64605E-01, 1.09063E-01, 5.22814E-02, 1.73224E-02, 5.53043E-03, 2.53055E-03, 0.00000E+00, 4.24842E-04, 6.75380E-03, 1.75110E-02, 2.99658E-02, 4.23889E-02, 5.39030E-02, 6.42958E-02, 8.28397E-02, 9.12342E-02, 9.90613E-02, 1.13550E-01, 1.26586E-01, 1.54685E-01, 1.78052E-01, 2.15774E-01, 3.45174E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def si_xenonnT_2303(mdm = 100, extrapol=False):
    """ Spin independent DM-Nucleon [cm^2] ref: 2303.14729 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"Xenon-nT SI 2303.14729"
    mass_min = 6.00000E+00
    mass_max = 1.00000E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -1.07271E+02
    fmax = -1.00188E+02 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.04745E+00, -2.87290E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.07790E-02, 3.87786E-02, 5.46553E-02, 6.88576E-02, 8.17051E-02, 9.34339E-02, 1.04223E-01, 1.23513E-01, 1.40385E-01, 1.62292E-01, 1.81131E-01, 1.92371E-01, 1.97657E-01, 2.08319E-01, 2.16947E-01, 2.37726E-01, 2.55725E-01, 2.71602E-01, 2.85804E-01, 2.98652E-01, 3.10381E-01, 3.31160E-01, 3.40460E-01, 3.49159E-01, 3.65036E-01, 3.79238E-01, 4.09317E-01, 4.33894E-01, 4.72672E-01, 5.96185E-01, 6.89619E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 7.97035E-01, 6.44082E-01, 5.25830E-01, 4.32295E-01, 3.57236E-01, 2.96198E-01, 2.45972E-01, 1.69528E-01, 1.15591E-01, 6.17076E-02, 2.82489E-02, 1.30746E-02, 7.02650E-03, 0.00000E+00, 1.57395E-03, 9.19081E-03, 1.90173E-02, 2.94945E-02, 3.98662E-02, 4.98734E-02, 5.93614E-02, 7.68373E-02, 8.49605E-02, 9.26982E-02, 1.07186E-01, 1.20511E-01, 1.49683E-01, 1.74245E-01, 2.13795E-01, 3.39125E-01, 4.35052E-01, 7.60161E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label