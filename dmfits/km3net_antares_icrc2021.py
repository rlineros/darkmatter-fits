""" FITTING FUNCTIONS FOR HESS """

import numpy as np
from scipy.interpolate import PchipInterpolator

### Data from icrc2021.537

@np.vectorize
def id_bb_km3netantares_icrc2021(mdm = 100, extrapol=False):
    """ Annihilation cross section into b quarks  [cm^3 s^-1] ref: icrc2021.537 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"KM3Net+Antares $b \bar{b}$ ICRC2021.537"
    mass_min = 5.04606E+01
    mass_max = 9.85154E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.17133E+01
    fmax = -4.67144E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [6.32187E-01, -4.87190E-01 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.07286E-02, 4.36109E-02, 6.24551E-02, 8.12994E-02, 1.00144E-01, 1.18988E-01, 1.37832E-01, 1.56676E-01, 1.75520E-01, 1.94365E-01, 2.13209E-01, 2.32053E-01, 2.50897E-01, 2.69742E-01, 2.88586E-01, 3.07430E-01, 3.26274E-01, 3.45118E-01, 3.63963E-01, 3.82807E-01, 4.01651E-01, 4.20495E-01, 4.39340E-01, 4.58184E-01, 4.77028E-01, 4.95872E-01, 5.14716E-01, 5.33561E-01, 5.52405E-01, 5.71249E-01, 5.90093E-01, 6.08938E-01, 6.27782E-01, 6.46626E-01, 6.65470E-01, 6.84314E-01, 7.03159E-01, 7.22003E-01, 7.40847E-01, 7.59691E-01, 7.78536E-01, 7.97380E-01, 8.16224E-01, 8.35068E-01, 8.53912E-01, 8.72757E-01, 8.91601E-01, 9.10445E-01, 9.29289E-01, 9.48134E-01, 9.66978E-01, 9.85822E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.40909E-01, 8.72701E-01, 8.18306E-01, 7.63149E-01, 7.21805E-01, 6.93797E-01, 6.65600E-01, 6.31496E-01, 5.87008E-01, 5.31089E-01, 4.75741E-01, 4.29158E-01, 3.86099E-01, 3.47899E-01, 3.14367E-01, 2.82549E-01, 2.55875E-01, 2.29488E-01, 2.03767E-01, 1.79475E-01, 1.58898E-01, 1.43275E-01, 1.28033E-01, 1.16316E-01, 1.07361E-01, 9.74536E-02, 8.62126E-02, 7.48764E-02, 6.36354E-02, 5.23944E-02, 4.11534E-02, 3.06746E-02, 2.38156E-02, 1.66710E-02, 1.00026E-02, 3.33419E-03, 0.00000E+00, 5.90628E-03, 1.21936E-02, 1.82904E-02, 2.43872E-02, 3.06746E-02, 3.65808E-02, 4.33445E-02, 5.25850E-02, 6.43975E-02, 7.64959E-02, 8.83084E-02, 1.00312E-01, 1.12219E-01, 1.24222E-01, 1.36130E-01, 1.44843E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_mu_km3netantares_icrc2021(mdm = 100, extrapol=False):
    """ Annihilation cross section into muons  [cm^3 s^-1] ref: icrc2021.537 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"KM3Net+Antares $\mu^+ \mu^-$ ICRC2021.537"
    mass_min = 5.01358E+01
    mass_max = 1.00000E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.43620E+01
    fmax = -5.24022E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [3.46091E+00, -2.60174E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.10729E-02, 4.61277E-02, 6.49188E-02, 8.37099E-02, 1.02501E-01, 1.21292E-01, 1.40083E-01, 1.58874E-01, 1.77665E-01, 1.96457E-01, 2.15248E-01, 2.34039E-01, 2.52830E-01, 2.71621E-01, 2.90412E-01, 3.09203E-01, 3.27994E-01, 3.46785E-01, 3.65576E-01, 3.84368E-01, 4.03159E-01, 4.21950E-01, 4.40741E-01, 4.59532E-01, 4.78323E-01, 4.97114E-01, 5.15905E-01, 5.34696E-01, 5.53488E-01, 5.75187E-01, 5.99570E-01, 6.31336E-01, 6.54602E-01, 6.73393E-01, 6.92184E-01, 7.10975E-01, 7.29766E-01, 7.48557E-01, 7.67348E-01, 7.86139E-01, 8.04930E-01, 8.23722E-01, 8.42513E-01, 8.61304E-01, 8.80095E-01, 8.98886E-01, 9.17677E-01, 9.35573E-01, 9.53632E-01, 9.78077E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.08296E-01, 8.06240E-01, 7.29455E-01, 6.65549E-01, 6.48054E-01, 6.43923E-01, 6.42951E-01, 6.24241E-01, 6.08446E-01, 5.38708E-01, 4.48559E-01, 3.96073E-01, 3.52092E-01, 3.16373E-01, 2.85513E-01, 2.53924E-01, 2.22822E-01, 1.91233E-01, 1.67663E-01, 1.51140E-01, 1.38018E-01, 1.27570E-01, 1.17364E-01, 1.04486E-01, 8.99062E-02, 7.97006E-02, 6.87661E-02, 6.07474E-02, 4.15512E-02, 3.50816E-02, 0.00000E+00, 9.23361E-03, 2.28410E-02, 4.15512E-02, 5.63736E-02, 8.38315E-02, 1.24168E-01, 1.74710E-01, 2.15289E-01, 2.55139E-01, 2.91345E-01, 3.36784E-01, 3.74204E-01, 4.33737E-01, 4.84036E-01, 5.39923E-01, 5.80988E-01, 6.34446E-01, 6.87948E-01, 7.85829E-01, 8.62371E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_nu_km3netantares_icrc2021(mdm = 100, extrapol = False):
    """ Annihilation cross section into tau  [cm^3 s^-1] ref: icrc2021.537 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"KM3Net+Antares $\nu \bar{\nu}$ ICRC2021.537"
    mass_min = 4.97791E+01
    mass_max = 9.85154E+04
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.56882E+01
    fmax = -5.33368E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [3.33283E+00, -2.33119E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.65138E-02, 4.53243E-02, 6.41347E-02, 8.29452E-02, 1.01756E-01, 1.20566E-01, 1.39377E-01, 1.58187E-01, 1.76997E-01, 1.95808E-01, 2.14618E-01, 2.33429E-01, 2.52239E-01, 2.71050E-01, 2.89860E-01, 3.08671E-01, 3.27481E-01, 3.46292E-01, 3.65102E-01, 3.83913E-01, 4.02723E-01, 4.21534E-01, 4.40344E-01, 4.59154E-01, 4.77965E-01, 4.96775E-01, 5.15586E-01, 5.34396E-01, 5.53207E-01, 5.72017E-01, 5.90828E-01, 6.09638E-01, 6.28449E-01, 6.47259E-01, 6.66070E-01, 6.84880E-01, 7.03690E-01, 7.22501E-01, 7.41311E-01, 7.60122E-01, 7.78932E-01, 7.97743E-01, 8.16553E-01, 8.35364E-01, 8.54174E-01, 8.72985E-01, 8.91795E-01, 9.10606E-01, 9.29416E-01, 9.48226E-01, 9.67037E-01, 9.85847E-01, 1.00000E+00 ]
        vval = [3.81132E-01, 3.61286E-01, 3.48932E-01, 3.37592E-01, 3.24226E-01, 3.42857E-01, 3.93283E-01, 4.41886E-01, 4.34798E-01, 3.96928E-01, 3.56830E-01, 3.15112E-01, 2.87165E-01, 2.63269E-01, 2.40182E-01, 2.17906E-01, 1.98667E-01, 1.86111E-01, 1.74365E-01, 1.66669E-01, 1.71327E-01, 1.63024E-01, 1.41760E-01, 1.21306E-01, 8.14108E-02, 2.57193E-02, 2.63269E-03, 6.48046E-03, 1.11383E-02, 1.11383E-02, 4.05029E-03, 1.41760E-03, 0.00000E+00, 2.40992E-02, 4.92110E-02, 7.43228E-02, 9.98396E-02, 1.31229E-01, 1.77808E-01, 2.24183E-01, 2.70762E-01, 3.17543E-01, 3.64121E-01, 4.10902E-01, 4.57480E-01, 5.13577E-01, 5.76559E-01, 6.39743E-01, 7.03130E-01, 7.66315E-01, 8.29297E-01, 8.92684E-01, 9.55666E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_tau_km3netantares_icrc2021(mdm = 100, extrapol=False):
    """ Annihilation cross section into tau  [cm^3 s^-1] ref: icrc2021.537 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"KM3Net+Antares $\tau^+\tau^-$ ICRC2021.537"
    mass_min = 4.94418E+01
    mass_max = 1.00888E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.42720E+01
    fmax = -5.15875E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [2.14556E+00, -1.54928E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.72995E-02, 4.60344E-02, 6.47694E-02, 8.35043E-02, 1.02239E-01, 1.20974E-01, 1.39709E-01, 1.58444E-01, 1.77179E-01, 1.95914E-01, 2.14649E-01, 2.33384E-01, 2.52119E-01, 2.70854E-01, 2.89589E-01, 3.08324E-01, 3.27059E-01, 3.45794E-01, 3.64529E-01, 3.83263E-01, 4.01998E-01, 4.20733E-01, 4.39468E-01, 4.58203E-01, 4.76938E-01, 4.95673E-01, 5.14408E-01, 5.33143E-01, 5.51878E-01, 5.70613E-01, 5.89348E-01, 6.10313E-01, 6.26818E-01, 6.45553E-01, 6.64288E-01, 6.83023E-01, 7.01758E-01, 7.20492E-01, 7.39227E-01, 7.57962E-01, 7.76697E-01, 7.95432E-01, 8.14167E-01, 8.32902E-01, 8.51637E-01, 8.70372E-01, 8.89107E-01, 9.07842E-01, 9.26577E-01, 9.45312E-01, 9.64047E-01, 9.82782E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 9.21916E-01, 8.67634E-01, 8.13351E-01, 7.65810E-01, 7.26961E-01, 6.82257E-01, 6.37199E-01, 5.76708E-01, 5.08411E-01, 4.46323E-01, 3.87428E-01, 3.34033E-01, 2.81702E-01, 2.43917E-01, 2.17485E-01, 1.92650E-01, 1.71185E-01, 1.49898E-01, 1.31626E-01, 1.17612E-01, 1.06082E-01, 9.86311E-02, 9.10031E-02, 8.21334E-02, 7.25541E-02, 6.15557E-02, 5.26860E-02, 4.32841E-02, 2.66091E-02, 1.31272E-02, 3.72527E-03, 0.00000E+00, 2.30612E-03, 9.22449E-03, 1.63202E-02, 2.73187E-02, 4.52355E-02, 7.39733E-02, 1.02356E-01, 1.30739E-01, 1.59477E-01, 1.88215E-01, 2.16598E-01, 2.45336E-01, 2.80460E-01, 3.20374E-01, 3.59400E-01, 3.99136E-01, 4.39227E-01, 4.78609E-01, 5.17813E-01, 5.57904E-01, 5.98279E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label

@np.vectorize
def id_ww_km3netantares_icrc2021(mdm = 100, extrapol=False):
    """ Annihilation cross section into W-boson [cm^3 s^-1] ref: icrc2021.537 """
    """ extrapol : if True the fitting function return values beyond max and min """

    label = r"KM3Net+Antares $W^+W^-$ ICRC2021.537"
    mass_min = 8.84811E+01
    mass_max = 1.00227E+05
    if (mdm < mass_min) :
        return float("Inf"), mass_min, mass_max, label

    fmin = -5.35334E+01
    fmax = -5.07614E+01 

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.90258E+00, -1.28645E+00 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf"), mass_min, mass_max, label
    else:
        # Data for interpolation
        uval = [0.00000E+00, 2.90042E-02, 4.93071E-02, 6.96101E-02, 8.99130E-02, 1.10216E-01, 1.30519E-01, 1.50822E-01, 1.71125E-01, 1.91428E-01, 2.11731E-01, 2.32034E-01, 2.52336E-01, 2.72639E-01, 2.92942E-01, 3.13245E-01, 3.33548E-01, 3.53851E-01, 3.74154E-01, 3.94457E-01, 4.14760E-01, 4.35063E-01, 4.55366E-01, 4.75669E-01, 4.95972E-01, 5.16275E-01, 5.36578E-01, 5.56880E-01, 5.77183E-01, 5.97486E-01, 6.17789E-01, 6.38092E-01, 6.58395E-01, 6.78698E-01, 6.99001E-01, 7.19304E-01, 7.39607E-01, 7.59910E-01, 7.80213E-01, 8.00516E-01, 8.20819E-01, 8.41121E-01, 8.61424E-01, 8.81727E-01, 9.02030E-01, 9.22333E-01, 9.42636E-01, 9.62939E-01, 9.83242E-01, 1.00000E+00 ]
        vval = [1.00000E+00, 8.76310E-01, 7.62584E-01, 6.49373E-01, 5.36334E-01, 4.42536E-01, 3.79144E-01, 3.20563E-01, 2.79333E-01, 2.43257E-01, 2.14053E-01, 1.89486E-01, 1.67841E-01, 1.50146E-01, 1.33826E-01, 1.19911E-01, 1.08916E-01, 1.05137E-01, 1.05137E-01, 1.05137E-01, 9.51727E-02, 8.28036E-02, 7.42141E-02, 6.87167E-02, 6.13297E-02, 4.74145E-02, 2.88610E-02, 1.08229E-02, 0.00000E+00, 1.37433E-02, 3.00636E-02, 4.60402E-02, 6.18450E-02, 8.17729E-02, 1.10119E-01, 1.38808E-01, 1.66982E-01, 1.95499E-01, 2.23845E-01, 2.52019E-01, 2.80708E-01, 3.15410E-01, 3.53204E-01, 3.91685E-01, 4.30510E-01, 4.68648E-01, 5.06442E-01, 5.44236E-01, 5.83233E-01, 6.17981E-01 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out, mass_min, mass_max, label