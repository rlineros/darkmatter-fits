"""constains fit for results of Antiproton bounds """

### last modification: 05/feb/2024

import numpy as np

# Data taken from 2401.10329
@np.vectorize
def id_bb_DLTorre_2401(mdm : float = 200 , extrapol : bool = False ) -> float:
    """ Annihilation cross section into b-quarks [cm^3 s^-1] ref: 2401.10329 """
    """ extrapol : if True the fitting function return values beyond max and min """

    mass_min = 2.00116E+01
    mass_max = 1.50245E+03
    if (mdm < mass_min) :
        return float("Inf")

    fmin = -6.17630E+01
    fmax = -5.70336E+01

    x = np.log(mdm)
    xmin = np.log(mass_min)
    xmax = np.log(mass_max)
    u = (x-xmin)/(xmax-xmin)

    if (mass_max < mdm) :
        if (extrapol) :
            # Data for linear extrapolation
            zzf = [1.06366E+00, -6.53263E-02 ]
            ppf = np.poly1d(zzf)
            v = ppf(u)
        else:
            return float("Inf")
    else:
        # Data for interpolation
        uval = [0.00000E+00, 3.17743E-02, 5.83800E-02, 9.77860E-02, 1.39655E-01, 1.81523E-01, 2.23392E-01, 2.65261E-01, 2.89663E-01, 3.19519E-01, 3.37514E-01, 3.57116E-01, 3.79809E-01, 3.99839E-01, 4.15270E-01, 4.32736E-01, 4.50730E-01, 4.71321E-01, 4.96017E-01, 5.11615E-01, 5.33186E-01, 5.59465E-01, 5.75909E-01, 5.99625E-01, 6.19914E-01, 6.39391E-01, 6.79584E-01, 7.19317E-01, 7.58739E-01, 7.88936E-01, 8.20919E-01, 8.54707E-01, 8.95755E-01, 9.21113E-01, 9.60267E-01, 1.00000E+00 ]
        vval = [0.00000E+00, 6.20952E-02, 1.15764E-01, 1.91262E-01, 2.63801E-01, 3.27165E-01, 3.92167E-01, 4.52728E-01, 4.81336E-01, 4.44943E-01, 4.43460E-01, 4.22495E-01, 4.01639E-01, 4.11081E-01, 4.13475E-01, 4.40107E-01, 4.68710E-01, 5.09960E-01, 5.57087E-01, 5.78011E-01, 6.02853E-01, 6.14368E-01, 5.98119E-01, 5.91402E-01, 5.53931E-01, 5.10291E-01, 4.56485E-01, 4.92847E-01, 5.65213E-01, 6.41728E-01, 7.19324E-01, 7.95642E-01, 8.74370E-01, 9.16116E-01, 9.52700E-01, 1.00000E+00 ]
        v = np.interp(u,uval,vval)

    out = (fmax-fmin)*v + fmin
    out = np.exp(out)
    return out

setattr(id_bb_DLTorre_2401,"label", r"De La Torre et al. $b \bar{b}$ 2401.10329")
setattr(id_bb_DLTorre_2401,"url", "https://inspirehep.net/literature/2749482")
setattr(id_bb_DLTorre_2401,"mass_min", 2.00116E+01)
setattr(id_bb_DLTorre_2401,"mass_max", 1.50245E+03)